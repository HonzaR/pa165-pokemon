package property;

/**
 *
 * @author Jan Rychnovský, 430642
 */
public enum Type {
    
//    NORMAL, FIRE, WATER, GRASS, ELECTRIC, ICE, FIGHTING, POISON, GROUND,
//    FLYING, PSYCHIC, BUG, ROCK, GHOST, DARK, DRAGON, STEEL, FAIRY
    
    
    NORMAL("Type.NORMAL", "normal"),
    FIRE("Type.FIRE", "fire"),
    WATER("Type.WATER", "water"),
    GRASS("Type.GRASS", "grass"),
    ELECTRIC("Type.ELECTRIC", "electric"),
    ICE("Type.ICE", "ice"),
    FIGHTING("Type.FIGHTING", "fighting"),
    POISON("Type.POISON", "poison"),
    GROUND("Type.GROUND", "groud"),
    FLYING("Type.FLYING", "flying"),
    PSYCHIC("Type.PSYCHIC", "psychic"),
    BUG("Type.BUG", "bug"),
    ROCK("Type.ROCK", "rock"),
    GHOST("Type.GHOST", "ghost"),
    DARK("Type.DARK", "dark"),
    DRAGON("Type.DRAGON", "dragon"),
    STEEL("Type.STEEL", "steel"),
    FAIRY("Type.FAIRY", "fairy");

    private final String localisation;
    private final String value;

    Type(String localisation, String value) {
        this.localisation= localisation;
        this.value =  value;
    }

    @Override
    public String toString() {
        return localisation;
    }

    public String getTypeString() {
        return value;
    }

    public static Type parseType(String typeString) {
        for (Type type : Type.values()) {
            if (type.value.equals(typeString)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unsupported type " + typeString);
    }
    
}
