/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.pa165_rest_client;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;




/**
 *
 * @author Tomas Sekera
 */
public class TrainerRest {
    
    
    private static final HttpAuthenticationFeature authentication = HttpAuthenticationFeature.basic("rest", "rest");
    /**
     * Constructor
     * @param args passed arguments
     */
    TrainerRest(String[] args) {
        
        switch(args[1])
        {
            case "create":
                createOperation(args);
                break;
            case "update":
                updateOperation(args);
                break;
            case "delete":
                deleteOperation(args);
                break;
            case "findAllTrainers":
                findAllTrainersOperation();
                break;
            case "findTrainerById":
                findTrainerByIdOperation(args);
                break;
            default:
                System.out.println( "Wrong arguments! Use help for info");
        }
    }
    /**
     * Create operation
     * @param args passed arguments
     */
    private void createOperation (String[] args)
    {
        //name, surname, birthdate
        if(args.length < 5)
        {
            System.out.println( "Wrong arguments! It should be: trainer create 'name' 'surname' 'birthdate'" );
        }
        
        TrainerDTO trainer = new TrainerDTO();
        trainer.setName(args[2]);
        trainer.setSurName(args[3]);
        Date date;
        
        DateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        try{
            date = formater.parse(args[4]);
            trainer.setBirthDate(date);
        }
        catch(ParseException e)
        {
            System.out.println("Wrong date format, it should be: dd-mm-yyyy");
        }
        
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/trainer/");
        webTarget.register(authentication);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(trainer, MediaType.APPLICATION_JSON_TYPE));
        
        if (response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            System.out.println("Trainer was created");
        } else {
            System.out.println("Error: " + response.getStatus());
        }
        
    }
    
    /**
     * Update operation
     * @param args passed arguments
     */
    private void updateOperation (String[] args)
    {
        if (args.length < 6) {
            System.out.println( "Wrong arguments! It should be: trainer update 'id' 'name' 'surname' 'birthdate'" );
        }
        
        TrainerDTO trainer = new TrainerDTO();
        Long id = null;
        try {
            id = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("id is not a number");
        }
        trainer.setId(id);
        trainer.setName(args[3]);
        trainer.setSurName(args[4]);
        
        Date date;
        
        DateFormat formater = new SimpleDateFormat("dd-mm-yyyy");
        try{
            date = formater.parse(args[5]);
            trainer.setBirthDate(date);
        }
        catch(ParseException e)
        {
            System.out.println("Wrong date format, it should be: dd-mm-yyyy");
        }
        
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/trainer/" + id.toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).put(Entity.entity(trainer, MediaType.APPLICATION_JSON_TYPE));
    
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            System.out.println("Trainer updated");
        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error: trainer with id: " + id + " not found");
        } else {
            System.out.println("Error: " + response.getStatus());
        }
    }
    
    /**
     * Delete operation
     * @param args passed arguments
     */
    private void deleteOperation (String[] args)
    {
         if (args.length < 3) {
            System.out.println( "Wrong arguments! It should be: trainer delete 'id'" );
        }

        Long id = null;
        
        try {
            id = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong arguments! Id have to be number!");
        }

        Client client = ClientBuilder.newBuilder().build();
                WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/trainer/" + id.toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);

        Response response = invocationBuilder.delete();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            System.out.println("Trainer successfully deleted");

        } else if (response.getStatus() == Response.Status.CONFLICT.getStatusCode()) {
            List<PokemonDTO> pokemonList = response.readEntity(new GenericType<List<PokemonDTO>>() {
            });

            System.out.println("Cannot delete this trainer, because he has this pokemons :");
            for (PokemonDTO b : pokemonList) {
                System.out.println(b.getName());
            }

        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("This id: " + id + " doesnt exist! Server returned: " + response.getStatus());
            System.out.println("Please use \"trainer findAllTrainers\" to list all trainers");
        } else {
            System.out.println("Server error code: " + response.getStatus());
        }
    }
    
    /**
     * Find all trainers opeation
     */
    private void findAllTrainersOperation ()
    {
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/trainer/");
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            List<TrainerDTO> trainerList = response.readEntity(new GenericType<List<TrainerDTO>>() {
            });
  
            for (TrainerDTO b : trainerList) {
                System.out.println(b);
            }
        } else {
            System.out.println("Server error code: " + response.getStatus());
        }
    }
    
    /**
     * Find trainer by id operation
     * @param args passed arguments
     */
    private void findTrainerByIdOperation (String[] args)
    {
        if (args.length < 3) {
            System.out.println("Wrong arguments! It should be: trainer findTrainerById 'id'" );
        }

        Long id = null;        
        try {
            id = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong arguments! Id has to be a number!");
            System.exit(1);
        }
        
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/trainer/" + id.toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);

        Response response = invocationBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            TrainerDTO trainerDto = response.readEntity(TrainerDTO.class);
            System.out.println("Pokemon found. " + trainerDto);
        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error - trainer was not found, wrong id: " + id + " Server returned: " + response.getStatus());
            System.out.println("Please use \"trainer findAllTrainers\" to list all trainers");
            System.exit(1);
        } else {
            System.out.println("Error on server, server returned " + response.getStatus());
            System.exit(1);
        }
    }  
    
}
