package cz.muni.cz.pa165_rest_client;

public class App 
{
    public static void main( String[] args )
    {
        
        //Print help if no args
        if (args.length == 0) {
            System.out.println( "Help for Rest client:\n"
                    + "Usable entities are: trainer, pokemon\n"
                    + "Pattern is: entity operation operation_arguments\n"
                    + "Example usage with arguments: trainer create Petr Novak 15-01-1989\n\n"
                    + "List of all operations for trainer:\n\n"
                    + "create 'name' 'surname' 'birthdate'        Creates new trainer.\n"
                    + "update 'id' 'name' 'surname' 'birthdate'   Update trainer with given id.\n"
                    + "delete 'id'                                Delete trainer with given id.\n"
                    + "findTainerById 'id'                        Find trainer with given id.\n"
                    + "findAllTrainers                            Show list of all trainers.\n"
                    
                    + "\n\n\n"
                    + "List of all arguments for pokemon:\n\n"
                    + "create 'name' 'nick' 'type' 'level' 'trainerId'      Creates new pokemon.\n"
                    + "update 'id' 'name' 'nick' 'type' 'level' 'trainerId' Update pokemon with given id.\n"
                    + "delete 'id'                                          Delete pokemon with given id.\n"
                    + "findAll                                              Show list of all pokemons.\n"
                    + "findById 'id'                                        Find pokemon with given id.\n");
            //System.exit(0);
        }
        else if (args.length < 2)
        {
            System.out.println( "Wrong arguments!" );
        }
        else
        {
            switch (args[0]) {
                case "trainer":
                    new TrainerRest(args);
                    break;
                case "pokemon":
                    new PokemonRest(args);
                    break;
                default:
                    System.out.println( "Wrong arguments!" );
                    //System.exit(1);
                    break;
            }
        }
    }
}
