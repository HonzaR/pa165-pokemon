package cz.muni.cz.pa165_rest_client;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.TrainerService;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import property.Type;

/**
 *
 * @author Jan Rychnovský
 */
public class PokemonRest {

    private static final HttpAuthenticationFeature authentication = HttpAuthenticationFeature.basic("rest", "rest");
    private TrainerService trnserv;
    /**
     * Constructor
     * @param args passed arguments
     */
    PokemonRest(String[] args) {
        
        switch(args[1])
        {
            case "create":
                createOperation(args);
                break;
            case "update":
                updateOperation(args);
                break;
            case "delete":
                deleteOperation(args);
                break;
            case "findAllPokemons":
                findAllPokemonsOperation();
                break;
            case "findPokemonById":
                findPokemonByIdOperation(args);
                break;          
            default:
                System.out.println( "Wrong arguments! Use help for info");
        }
    }
    
    /**
     * Create operation
     * @param args passed arguments
     */
    private void createOperation (String[] args)
    {
        //name, nick, type, level, trainer
        if(args.length < 7)
        {
            System.out.println( "Wrong arguments! It should be: pokemon create 'name' 'nick' 'type' 'level' 'trainerId'" );
        }      
        
        PokemonDTO pokemon = new PokemonDTO();
        pokemon.setName(args[2]);
        pokemon.setNick(args[3]);
        pokemon.setLevel(Integer.parseInt(args[5]));
        String typeString = (args[4]);
        Long trainerId = null;
                
        // convert string to type of pokemon
        Type typeEnum = null;
        try {
            pokemon.setType(Type.parseType(typeString));
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error: Unsupported type \nPossible types are: \n\n");

            for (Type type : Type.values()) {
                sb.append(type.getTypeString());
                sb.append("\n");
            }
            System.out.println(sb);
            System.exit(1);
        }
        
        // getting trainers id
        try {
            trainerId = Long.parseLong(args[6]);
        } catch(NumberFormatException e) {
            System.out.println("Id is not a number");
            System.exit(1);
        }
        // find pokemons trainer with given id
        Client clientTr = ClientBuilder.newBuilder().build();
        WebTarget webTargetTr = clientTr.target("http://localhost:8080/pa165/rest/trainer/" + trainerId.toString());
        webTargetTr.register(authentication);
        Invocation.Builder invocationBuilderTr = webTargetTr.request(MediaType.APPLICATION_JSON);
        invocationBuilderTr.header("accept", MediaType.APPLICATION_JSON);

        Response responseTr = invocationBuilderTr.get();

        if (responseTr.getStatus() == Response.Status.OK.getStatusCode()) {
            TrainerDTO trainerDto = responseTr.readEntity(TrainerDTO.class);
            pokemon.setTrainer(trainerDto);
        } else if (responseTr.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error - trainer was not found, wrong id: " + trainerId + " Server returned: " + responseTr.getStatus());
            System.out.println("Please use \"trainer findAllTrainers\" to list all trainers");
            System.exit(1);
        } else {
            System.out.println("Error on server, server returned " + responseTr.getStatus());
            System.exit(1);
        }
        
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/pokemon/");
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.entity(pokemon, MediaType.APPLICATION_JSON_TYPE));

        if (response.getStatus() == Response.Status.CREATED.getStatusCode()) {
            System.out.println("Pokemon was created");
        } else {
            System.out.println("Server error code: " + response.getStatus());
            System.exit(1);
        }        
    } 
    
    
    
    /**
     * Update operation
     * @param args passed arguments
     */    
    private void updateOperation (String[] args)
    {
        if (args.length < 8) {
            System.out.println( "Wrong arguments! It should be: pokemon update 'id' 'name' 'nick' 'type' 'level' 'trainerId'" );
            System.exit(1);
        }
        
        PokemonDTO pokemon = new PokemonDTO();
        Long trainerId = null;
        
        try {
            pokemon.setId(Long.parseLong(args[2]));
            trainerId = Long.parseLong(args[7]);
        } catch (NumberFormatException e) {
            System.out.println("id is not a number");
            System.exit(1);
        }

        pokemon.setName(args[3]);
        pokemon.setNick(args[4]);
        pokemon.setLevel(Integer.parseInt(args[6]));
        String typeString = (args[5]);
        
        // convert string to type of pokemon
        Type typeEnum = null;
        try {
            pokemon.setType(Type.parseType(typeString));
        } catch (IllegalArgumentException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Error: Unsupported type \nPossible types are: \n\n");

            for (Type type : Type.values()) {
                sb.append(type.getTypeString());
                sb.append("\n");
            }
            System.out.println(sb);
            System.exit(1);
        }
        
        // find pokemons trainer with given id
        Client clientTr = ClientBuilder.newBuilder().build();
        WebTarget webTargetTr = clientTr.target("http://localhost:8080/pa165/rest/trainer/" + trainerId.toString());
        webTargetTr.register(authentication);
        Invocation.Builder invocationBuilderTr = webTargetTr.request(MediaType.APPLICATION_JSON);
        invocationBuilderTr.header("accept", MediaType.APPLICATION_JSON);

        Response responseTr = invocationBuilderTr.get();

        if (responseTr.getStatus() == Response.Status.OK.getStatusCode()) {
            TrainerDTO trainerDto = responseTr.readEntity(TrainerDTO.class);
            pokemon.setTrainer(trainerDto);
        } else if (responseTr.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error - trainer was not found, wrong id: " + trainerId + " Server returned: " + responseTr.getStatus());
            System.out.println("Please use \"trainer findAllTrainers\" to list all trainers");
            System.exit(1);
        } else {
            System.out.println("Error on server, server returned " + responseTr.getStatus());
            System.exit(1);
        }
        
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/pokemon/" + pokemon.getId().toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).put(Entity.entity(pokemon, MediaType.APPLICATION_JSON_TYPE));
        
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            System.out.println("Pokemon updated");
        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error: pokemon with id: " + pokemon.getId() + " not found");
            System.exit(1);
        } else {
            System.out.println("Error: " + response.getStatus());
            System.exit(1);
        }
    }
    
    /**
     * Delete operation
     * @param args passed arguments
     */
    private void deleteOperation (String[] args)
    {
         if (args.length < 3) {
            System.out.println("Wrong arguments! It should be: pokemon delete 'id'" );
            System.exit(1);
        }

        Long id = null;        
        try {
            id = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong arguments! Id has to be a number!");
            System.exit(1);
        }

        Client client = ClientBuilder.newBuilder().build();
                WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/pokemon/" + id.toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);

        Response response = invocationBuilder.delete();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            System.out.println("Pokemon successfully deleted");

        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Pokemon with this id: " + id + " doesnt exist! Server returned: " + response.getStatus());
            System.out.println("Please use \"pokemon findAllPokemons\" to list all pokemons");
            System.exit(1);
        } else {
            System.out.println("Server error code: " + response.getStatus());
            System.exit(1);
        }
    }
    
    /**
     * Find all pokemon opeation
     */
    private void findAllPokemonsOperation ()
    {
        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/pokemon/");
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            List<PokemonDTO> pokemonList = response.readEntity(new GenericType<List<PokemonDTO>>() {
            });
  
            for (PokemonDTO b : pokemonList) {
                System.out.println(b);
            }
        } else {
            System.out.println("Server error code: " + response.getStatus());
            System.exit(1);
        }
    }

    
    /**
     * Find pokemon by its id operation
     *
     * @param args passed arguments
     */
    private void findPokemonByIdOperation(String[] args) {
        
         if (args.length < 3) {
            System.out.println("Wrong arguments! It should be: pokemon findPokemonById 'id'" );
        }

        Long id = null;        
        try {
            id = Long.parseLong(args[2]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong arguments! Id has to be a number!");
            System.exit(1);
        }

        Client client = ClientBuilder.newBuilder().build();
        WebTarget webTarget = client.target("http://localhost:8080/pa165/rest/pokemon/" + id.toString());
        webTarget.register(authentication);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("accept", MediaType.APPLICATION_JSON);

        Response response = invocationBuilder.get();

        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            PokemonDTO pokemonDto = response.readEntity(PokemonDTO.class);
            System.out.println("Pokemon found. " + pokemonDto);
        } else if (response.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
            System.out.println("Error - pokemon was not found, wrong id: " + id + " Server returned: " + response.getStatus());
            System.out.println("Please use \"pokemon findAllPokemons\" to list all pokemons");
            System.exit(1);
        } else {
            System.out.println("Error on server, server returned " + response.getStatus());
            System.exit(1);
        }
    }
}
