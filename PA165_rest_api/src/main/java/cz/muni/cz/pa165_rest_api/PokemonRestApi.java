/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.pa165_rest_api;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import property.Type;

/**
 *
 * @author Martin Prokop
 */
@Path("pokemon")
public class PokemonRestApi {
    
    @Autowired
    PokemonService pokemonService;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(PokemonDTO pokemon) {
        pokemonService.createPokemon(pokemon);
        return Response.status(Response.Status.CREATED).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response update(@PathParam("id") Long id, PokemonDTO pokemon) {
        try {
            pokemonService.findPokemonById(id);
            pokemonService.updatePokemon(pokemon);
            return Response.status(Response.Status.OK).build();
        } catch (DAOException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        try {
            PokemonDTO pokemon = pokemonService.findPokemonById(id);
            pokemonService.deletePokemon(pokemon);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAllPokemons(@QueryParam("name") String name, @QueryParam("type") Type type) {
        List<PokemonDTO> pokemonDtoList;

        if (name != null & type != null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else if (name != null) {
            pokemonDtoList = pokemonService.findPokemonByName(name);

        } else if (type != null) {
            pokemonDtoList = pokemonService.findPokemonByType(type);

        } else {
            pokemonDtoList = pokemonService.findAllPokemon();
        }
        GenericEntity<List<PokemonDTO>> pokemonEntity = new GenericEntity<List<PokemonDTO>>(pokemonDtoList) {
        };
        return Response.status(Response.Status.OK).entity(pokemonEntity).build();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findPokemonById(@PathParam("id") Long id) {
        try {
            PokemonDTO pokemonDTO = pokemonService.findPokemonById(id);
            return Response.status(Response.Status.OK).entity(pokemonDTO).build();
        } catch (DAOException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
