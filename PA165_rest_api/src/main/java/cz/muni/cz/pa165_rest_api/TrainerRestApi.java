/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.pa165_rest_api;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.api.service.TrainerService;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Martin Prokop
 */
@Path("trainer")
public class TrainerRestApi {
    
    @Autowired
    TrainerService trainerService;
    
    @Autowired
    PokemonService pokemonService;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(TrainerDTO trainer) {
        trainerService.createTrainer(trainer);
        return Response.status(Response.Status.CREATED).build();
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response update(@PathParam("id") Long id, TrainerDTO trainer) {
        try {
            trainerService.findTrainerById(id);
            trainerService.updateTrainer(trainer);
            return Response.status(Response.Status.OK).build();
        } catch (DAOException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        try {
            TrainerDTO trainer = trainerService.findTrainerById(id);
            
            List<PokemonDTO> pokemonDtoList = pokemonService.findPokemonByTrainer(trainer);
            
            if (pokemonDtoList.isEmpty()) {
                trainerService.deleteTrainer(trainer);
                return Response.status(Response.Status.OK).build();
            }
            
            GenericEntity<List<PokemonDTO>> genericEntityList = new GenericEntity<List<PokemonDTO>>(pokemonDtoList) {
            };
            return Response.status(Response.Status.CONFLICT).entity(genericEntityList).build();
        } catch (DAOException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAllTrainers(@QueryParam("name") String name) {
        List<TrainerDTO> trainerDtoList;

        trainerDtoList = trainerService.findAllTrainer();
            
        GenericEntity<List<TrainerDTO>> trainerEntity = new GenericEntity<List<TrainerDTO>>(trainerDtoList) {
        };
        return Response.status(Response.Status.OK).entity(trainerEntity).build();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findTrainerById(@PathParam("id") Long id) {
        try {
            TrainerDTO trainerDTO = trainerService.findTrainerById(id);
            return Response.status(Response.Status.OK).entity(trainerDTO).build();
        } catch (DAOException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
    
}
