package cz.muni.cz;


import cz.muni.cz.pa165_rest_api.PokemonRestApi;
import cz.muni.cz.pa165_rest_api.TrainerRestApi;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author: Martin Prokop
 */
public class ApplicationConfig extends ResourceConfig {
    public ApplicationConfig() {
        register(PokemonRestApi.class);
        register(TrainerRestApi.class);
    }
}