# README #
### Running application ###
From directory ** PA165_aggregator_module** run command

```
#!java

mvn clean install
```
### Running server ###
From directory **PA165_web_module** run command
```
#!java

mvn tomcat7:run
```
Server is running on URL: **http://localhost:8080/pa165**

### SQL command###
It is necessary before using application to fill it with data.
From directory **PA165_backend_module** run command

```
#!java

mvn sql:execute
```


### Rest client - console###
From directory **PA165_rest_client** run commands for using REST client 
```
#!java

mvn exec:java -Dexec.args="arguments"
```

 Usable entities are: trainer, pokemon

                    Pattern is: entity operation operation_arguments

                    Example usage with arguments: trainer create Petr Novak 15-01-1989
                    Example usage with arguments: trainer findAllTrainers
                    

                    List of all operations for trainer:

                    create 'name' 'surname' 'birthdate'        Creates new trainer.
                    update 'id' 'name' 'surname' 'birthdate'   Update trainer with given id.
                    delete 'id'                                Delete trainer with given id.
                    findTainerById 'id'                        Find trainer with given id
                    findAllTrainers                            Show list of all trainers.
                    

                    List of all arguments for pokemon:

                    create 'name' 'nick' 'type' 'level' 'trainerId'      Creates new pokemon.
                    update 'id' 'name' 'nick' 'type' 'level' 'trainerId' Update pokemon with given id.
                    delete 'id'                                          Delete pokemon with given id."
                    findAllPokemons                                      Show list of all pokemons.
                    findPokemonById 'id'                                 Find pokemon with given id.