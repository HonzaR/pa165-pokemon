/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.api.service;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import java.util.List;
import property.Type;

/**
 * Interface for Pokemon service
 * 
 * @author Jan Rychnovský
 */
public interface PokemonService {

       
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method create Pokemon with
     * entity on DAO layer
     *
     * @param pokemon Pokemon to be created as DTO
     */
    void createPokemon(PokemonDTO pokemon);

     /**
     * Convert Pokemon DTO into Pokemon entity and perform method update Pokemon with
     * entity on DAO layer
     *
     * @param pokemon Pokemon as DTO
     */
    void updatePokemon(PokemonDTO pokemon);

     /**
     * method delete Pokemon with entity on DAO layer
     *
     * @param pokemon Pokemon to be deleted as DTO
     */
    void deletePokemon(PokemonDTO pokemon);

    /**
     * Method find Pokemon on DAO layer. Pokemon is find based on its id
     *
     * @param id Pokemon id
     * @return Pokemon as DTO
     */
    PokemonDTO findPokemonById(Long id);

     /**
     * Method findAllPokemons on DAO layer which returns list of all pokemons
     *
     * @return list of all Pokemons as DTO
     */
    List<PokemonDTO> findAllPokemon();
    
     /**
     * Method findPokemonByName on DAO layer which returns list of pokemons
     *
     * @param name
     * @return list of all Pokemons as DTO
     */
    List<PokemonDTO> findPokemonByName(String name); 
    
     /**
     * Method findPokemonByNick on DAO layer which returns concreate pokemon
     *
     * @param nick
     * @return  Pokemon as DTO
     */
    PokemonDTO findPokemonByNick(String nick); 
    
     /**
     * Method findPokemonByType on DAO layer which returns list of pokemons
     *
     * @param type
     * @return list of Pokemons as DTO
     */
    List<PokemonDTO> findPokemonByType(Type type); 
    
     /**
     * Method findPokemonByLevel on DAO layer which returns list of pokemons
     *
     * @param level
     * @return list of Pokemons as DTO
     */
    List<PokemonDTO> findPokemonByLevel(int level); 
    
     /**
     * Method findPokemonByTrainer on DAO layer which returns list of pokemons
     *
     * @param trainer
     * @return list of Pokemons as DTO
     */
    List<PokemonDTO> findPokemonByTrainer(TrainerDTO trainer);     
    
    
}

