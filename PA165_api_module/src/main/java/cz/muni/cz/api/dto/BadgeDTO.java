package cz.muni.cz.api.dto;

/**
 * Badge DTO
 * @author Tomas Sekera, 359902
 */
public class BadgeDTO {
    
    private Long id;
    private TrainerDTO trainer;
    private StadiumDTO stadium;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }

    public StadiumDTO getStadium() {
        return stadium;
    }

    public void setStadium(StadiumDTO stadium) {
        this.stadium = stadium;
    }
        
    public String getDescription() {
        return this.toString();
    }
    
     @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 97 * hash + (this.trainer != null ? this.trainer.hashCode() : 0);
        hash = 97 * hash + (this.stadium != null ? this.stadium.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BadgeDTO other = (BadgeDTO) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.trainer != other.trainer && (this.trainer == null || !this.trainer.equals(other.trainer))) {
            return false;
        }
        return !(this.stadium != other.stadium && (this.stadium == null || !this.stadium.equals(other.stadium)));
    }

    @Override
    public String toString() {
        return this.getTrainer().getName() + " " + this.getTrainer().getSurName() + ", " + this.getStadium().getCity();
    }      
    
//    public String toString() {
//        return "BadgeDTO {" + "id=" + id + ", trainer=" + trainer + ", stadium=" + stadium + '}';
//    }
    
    
}
