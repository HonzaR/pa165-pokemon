package cz.muni.cz.api.service;

import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import java.util.List;
import property.Type;

/**
 * Interface Stadium service
 * @author Tomáš Sládek, 359532
 */
public interface StadiumService {
    
   /**
     * Create new stadium
     * @param stadium Stadium to be created
     */
    public void createStadium(StadiumDTO stadium);
    
    /**
     * Update existing stadium
     * @param stadium Stadium to be updated
     */
    public void updateStadium(StadiumDTO stadium);
    
    /**
     * Delete existing stadium
     * @param stadium Stadium to be deleted
     */
    public void deleteStadium(StadiumDTO stadium);
    
    /**
     * Find Stadium by id
     * @param id Id of desired stadium
     * @return Stadium    
     */
    public StadiumDTO findStadiumById(Long id);
    
    /**
     * Find all stadiums
     * @return List of stadiums
     */
    public List<StadiumDTO> findAllStadiums();
    
    
    /**
     * Get stadium by trainer
     * @param trainer Desired trainer
     * @return  Stadium of the trainer
     */
    public StadiumDTO findStadiumByTrainer(TrainerDTO trainer);
    
    /**
     * List stadiums by city
     * @param city Desired city
     * @return List of stadiums which come from the same city
     */
    public List<StadiumDTO> findStadiumByCity(String city);
    
    /**
     * List stadiums by type
     * @param type Desired type
     * @return List of stadiums which are of the same type
     */
    public List<StadiumDTO> findStadiumByType(Type type);
    
}
