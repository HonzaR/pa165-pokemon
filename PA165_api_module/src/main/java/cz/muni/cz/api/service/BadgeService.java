package cz.muni.cz.api.service;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import java.util.List;

/**
 * Interface Badge service
 * @author Tomas Sekera, 359902
 */
public interface BadgeService {

   /**
     * Create new badge
     * @param badge Badge to be created
     */
    public void createBadge(BadgeDTO badge);
    
    /**
     * Update existing badge
     * @param badge Badge to be updated
     */
    public void updateBadge(BadgeDTO badge);
    
    /**
     * Delete existing badge
     * @param badge Badge to be deleted
     */
    public void deleteBadge(BadgeDTO badge);
    
    /**
     * Find Badge by id
     * @param id Id of desired badge
     * @return Badge    
     */
    public BadgeDTO findBadgeById(Long id);
    
    /**
     * Find all badges
     * @return List of badges
     */
    public List<BadgeDTO> findAllBadges();
    
    /**
     * List badges by stadium
     * @param stadium Desired stadium
     * @return List of badges which come from the same stadium
     */
    public List<BadgeDTO> findBadgeByStadium(StadiumDTO stadium);
    
    /**
     * List badges by trainer
     * @param trainer Desired trainer
     * @return List of badges which come from the same trainer
     */
    public List<BadgeDTO> findBadgeByTrainer(TrainerDTO trainer);
    
  

}
