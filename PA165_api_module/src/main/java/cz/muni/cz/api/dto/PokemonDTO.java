/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.api.dto;

import property.Type;

/**
 * DTO of Pokemon entity 
 * 
 * @author Jan Rychnovský
 */
public class PokemonDTO {

    private Long id;
    private String name;
    private String nickname;
    private Type type;
    private int level;
    private TrainerDTO trainer;

    /**
     * get the value of id
     * @return identification of the PokemonDTO
     */
    public Long getId() {
        return id;
    }

    /**
     * set the value of id
     * @param id identification of the PokemonDTO
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * get the value of name
     * @return trainer name
     */
    public String getName() {
        return name;
    }

    /**
     * set the trainer name value
     * @param name trainer name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * get the value of nickname
     * @return trainers nickname
     */
    public String getNick() {
        return nickname;
    }

    /**
     * set the trainers nickname
     * @param nickname trainers nickname
     */
    public void setNick(String nickname) {
        this.nickname = nickname;
    }

    /**
     * get the value of type
     * @return trainer type
     */
    public Type getType() {
        return type;
    }

    /**
     * set the value of type
     * @param type trainer type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * get the value of rank
     * @return trainer rank
     */
    public int getLevel() {
        return level;
    }
    
    /**
     * set the value of rank
     * @param level level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * get the Trainer DTO
     * @return trainer DTO
     */
    public TrainerDTO getTrainer() {
        return trainer;
    }

    /**
     * set the Trainer DTO
     * @param trainer trainer DTO
     */
    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }
        
    public String getDescription() {
        return this.toString();
    }
    
    /**
     * Return Hash code
     * @return hash code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    /**
     * Method compares two Pokemon DTO objects 
     * @return TRUE in case the the objects are same. FALSE in case objects are different
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PokemonDTO other = (PokemonDTO) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

     /**
     * Create string of Pokemon DTO's parameters 
     * @return parameters of Pokemon DTO in string
     */
    @Override
    public String toString() {
        return this.getName()+ " " + this.getNick()+ ", " + this.getType() + " level=" + this.getLevel() + " trainer=" + this.getTrainer().getName() + " " + this.getTrainer().getSurName();
    }  

//    @Override
//    public String toString() {
//        return "PokemonDTO{" + "id=" + id + ", name=" + name + ", nickname=" + nickname + ", type=" + type + ", rank=" + level + ", trainer=" + trainer + '}';
//    }
}
