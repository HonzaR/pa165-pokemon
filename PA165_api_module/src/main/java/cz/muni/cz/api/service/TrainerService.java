/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.api.service;

import cz.muni.cz.api.dto.TrainerDTO;
import java.util.List;

/**
 * Interface for Trainer service
 * 
 * @author Martin Prokop
 */
public interface TrainerService {
    
    /**
     * Create new trainer
     * @param trainer TrainerDTO to be created
     */
    public void createTrainer(TrainerDTO trainer);
    
    /**
     * Update existing trainer
     * @param trainer TrainerDTO to be updated
     */
    public void updateTrainer(TrainerDTO trainer);
    
    /**
     * Delete existing trainer
     * @param trainer TrainerDTO to be deleted
     */
    public void deleteTrainer(TrainerDTO trainer);    
    
    /**
     * Find trainer by id
     * @param id Id of desired trainer
     * @return TrainerDTO
     */
    public TrainerDTO findTrainerById(Long id);
    
    /**
     * Find all trainers
     * @return List of trainers
     */
    public List<TrainerDTO> findAllTrainer();
    
    /**
     * Find trainer by name
     * @param name Name of desired trainer
     * @return TrainerDTO
     */
    public TrainerDTO findTrainerByName(String name);
}
