
package cz.muni.cz.api.dto;

import property.Type;

/**
 * DTO of Stadium entity
 * 
 * @author Tomáš Sládek
 */
public class StadiumDTO {
    
    private Long id;   
    private String city; 
    private Type type;    
    private TrainerDTO trainer;
    private String description;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }
        
    public String getDescription() {
        return this.toString();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 47 * hash + (this.city != null ? this.city.hashCode() : 0);
        hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 47 * hash + (this.trainer != null ? this.trainer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StadiumDTO other = (StadiumDTO) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.city == null) ? (other.city != null) : !this.city.equals(other.city)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (this.trainer != other.trainer && (this.trainer == null || !this.trainer.equals(other.trainer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getCity() + ", " + this.getType() + ", " + this.getTrainer().getName() + " " + this.getTrainer().getSurName();
    }  
    
//    public String toString() {
//        return "StadiumDTO{" + "id=" + id + ", city=" + city + ", type=" + type + ", trainer=" + trainer.toString() + '}';
//    }
    
   
}
