/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.api.dto;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * DTO of trainer entity
 * 
 * @author Martin Prokop
 */
public class TrainerDTO {
    
    private Long id;
    private String name;
    private String surName;
    private java.util.Date birthDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Date getBirthDate() {
        return birthDate;
    }
    
    public Date getBirthDateWithoutTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");      
        try {
            Date dateWithoutTime = sdf.parse(sdf.format(birthDate));
            return dateWithoutTime;
        } catch (ParseException ex) {
            Logger.getLogger(TrainerDTO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return this.toString();
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 97 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 97 * hash + (this.surName != null ? this.surName.hashCode() : 0);
        hash = 97 * hash + (this.birthDate != null ? this.birthDate.hashCode() : 0);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrainerDTO other = (TrainerDTO) obj;
        if (this.id == null || !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.getName()+ " " + this.getSurName()+ ", " + this.getBirthDate().toString().substring(0,10) + " ID=" + this.getId();
    }     
    
//    public String toString() {
//        return "Trainer {" + id + "} "
//               + "[name = " + name + "[surname = " + surName + ", birthdate = " + birthDate.toString() +  "]";
//    }
    
      
    
}
