package cz.muni.cz.api.service;

import cz.muni.cz.api.dto.AccountDTO;
import org.springframework.security.access.annotation.Secured;
import java.util.List;

/*
 * Account Service interface
 *
 * @author: Tomas Sladek, 359532
 */
public interface AccountService {

    @Secured("ROLE_ADMIN")
    public void create(AccountDTO dto);

    @Secured("ROLE_ADMIN")
    public void update(AccountDTO dto);

    @Secured("ROLE_ADMIN")
    public void delete(AccountDTO dto);

    @Secured("ROLE_ADMIN")
    public List<AccountDTO> findAll();

    @Secured("ROLE_ADMIN")
    public AccountDTO findById(Long id);

    public AccountDTO findByName(String name);
}
