package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.api.service.BadgeService;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.api.service.StadiumService;
import cz.muni.cz.api.service.TrainerService;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.LocalizableError;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


/**
 * Stripes ActionBean for handling trainer operations.
 *
 * @author Martin Prokop
 */
@UrlBinding("/trainers/{$event}/{trainer.id}")
public class TrainerActionBean extends BaseActionBean implements ValidationErrorHandler {



    @SpringBean
    protected TrainerService trainerService;
    
    @SpringBean
    protected PokemonService pokemonService;
    
    @SpringBean
    protected StadiumService stadiumService;
    
    @SpringBean
    protected BadgeService badgeService;
    
    @SpringBean
    protected AccountService accountService;

    //--- part for showing a list of trainer ----
    private List<TrainerDTO> trainers;
    private Long trainerID;

    @DefaultHandler
    public Resolution list() {

        UserDetails u = (UserDetails)(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String name = u.getUsername();
        trainerID = accountService.findByName(name).getTrainerID();
        trainers = trainerService.findAllTrainer();
        return new ForwardResolution("/trainer/list.jsp");
    }

     public Long getTrainerID() {
        return trainerID;
    }
    
    
    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    //--- part for adding a trainer ----

    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "name", required = true),
            @Validate(on = {"add", "save"}, field = "surName", required = true),
            @Validate(on = {"add", "save"}, field = "birthDate", required = true)
    })
    private TrainerDTO trainer;

    public Resolution add() {

        trainerService.createTrainer(trainer);
        getContext().getMessages().add(new LocalizableMessage("trainer.add.message",escapeHTML(trainer.getName()),escapeHTML(trainer.getSurName())));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        trainers = trainerService.findAllTrainer();
        //return null to let the event handling continue
        return null;
    }

    public TrainerDTO getTrainer() {
        return trainer;
    }

    public void setTrainer(TrainerDTO trainer) {
        this.trainer = trainer;
    }

    //--- part for deleting a trainer ----

    public Resolution delete() {
        trainer = trainerService.findTrainerById(trainer.getId());
        List<PokemonDTO> pokemonDTOList = pokemonService.findPokemonByTrainer(trainer);
        StadiumDTO stadiumDTO = stadiumService.findStadiumByTrainer(trainer);
        List<BadgeDTO> badgeDTOList = badgeService.findBadgeByTrainer(trainer);

        // list is empty, brick is not contained in any building kit => delete brick
        if (pokemonDTOList.isEmpty() && stadiumDTO == null && badgeDTOList.isEmpty()) {
            trainerService.deleteTrainer(trainer);
            getContext().getMessages().add(new LocalizableMessage("trainer.delete.message",escapeHTML(trainer.getName()),escapeHTML(trainer.getSurName())));
        } else { // list is not empty == brick is used by some building kit
            
            if(!pokemonDTOList.isEmpty()){
                StringBuilder sb = new StringBuilder();
                sb.append("<ul>");
                for (PokemonDTO pokemon : pokemonDTOList) {
                    sb.append("<li>");
                    sb.append(pokemon.getName());
                    sb.append("</li>");                
                }
                sb.append("</ul>");

                getContext().getMessages().add(new LocalizableMessage("trainer.delete.pokemon", sb.toString()));
            }
            if(stadiumDTO != null){
                getContext().getMessages().add(new LocalizableMessage("trainer.delete.stadium", stadiumDTO.getCity() + " - " + stadiumDTO.getType()));
            }
            if(!badgeDTOList.isEmpty()){
                StringBuilder sb = new StringBuilder();
                sb.append("<ul>");
                for (BadgeDTO badge : badgeDTOList) {
                    sb.append("<li>");
                    sb.append(badge.getStadium() + " - " + badge.getTrainer());
                    sb.append("</li>");                
                }
                sb.append("</ul>");

                getContext().getMessages().add(new LocalizableMessage("trainer.delete.badge", sb.toString()));
            }
        }
        //only id is filled by the form
        
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for editing a trainer ----

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadTrainerFromDatabase() {
        String ids = getContext().getRequest().getParameter("trainer.id");
        if (ids == null) return;
        trainer = trainerService.findTrainerById(Long.parseLong(ids));
    }

    public Resolution edit() {

        return new ForwardResolution("/trainer/edit.jsp");
    }

    public Resolution save() {

        trainerService.updateTrainer(trainer);
        return new RedirectResolution(this.getClass(), "list");
    }
}
