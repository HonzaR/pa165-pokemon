package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.api.service.BadgeService;
import cz.muni.cz.api.service.StadiumService;
import cz.muni.cz.api.service.TrainerService;
import static cz.muni.cz.pa165_web_module.BaseActionBean.escapeHTML;
import java.util.ArrayList;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;


import java.util.List;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Stripes ActionBean for handling stadium operations.
 *
 * @author Tomáš Sládek, 359532
 */
@UrlBinding("/stadiums/{$event}/{stadium.id}")
public class StadiumActionBean extends BaseActionBean implements ValidationErrorHandler {



    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;
    @SpringBean
    protected TrainerService trainerService;
    @SpringBean
    protected BadgeService badgeService;
     @SpringBean
    protected AccountService accountService;

    //--- part for showing a list of stadiums ----
    private List<StadiumDTO> stadiums;
    private List<TrainerDTO> trainers;
     private Long trainerID;

    @DefaultHandler
    public Resolution list() {

        UserDetails u = (UserDetails)(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String name = u.getUsername();
        trainerID = accountService.findByName(name).getTrainerID();
        stadiums = stadiumService.findAllStadiums();

        boolean isAdmin = getContext().getRequest().isUserInRole("ROLE_ADMIN");
        if (isAdmin) {
            trainers = trainerService.findAllTrainer();
        } else {
            if (trainers == null) {
                trainers = new ArrayList<>();
            } else {
                trainers.clear();
            }
            TrainerDTO t = trainerService.findTrainerById(trainerID);
            trainers.add(t);
        }
        
        return new ForwardResolution("/stadium/list.jsp");
    }
    
     public Long getTrainerID() {
        return trainerID;
    }

    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }
    
    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    //--- part for adding a stadium ----

    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "city", required = true),
            @Validate(on = {"add", "save"}, field = "type", required = true),
            @Validate(on = {"add", "save"}, field = "trainer.id", required = true)
    })
    private StadiumDTO stadium;
    private TrainerDTO trainer;

    public Resolution add() {
        stadium.setTrainer(trainerService.findTrainerById(stadium.getTrainer().getId()));
        trainer = stadium.getTrainer();

        StadiumDTO stadiumDTO = stadiumService.findStadiumByTrainer(trainer);
        
        if (stadiumDTO == null) {
                    stadiumService.createStadium(stadium);
        getContext().getMessages().add(new LocalizableMessage("stadium.add.message",escapeHTML(stadium.getCity()),escapeHTML(stadium.getType().toString())));

        } else { // list is not empty == brick is used by some building kit
            getContext().getMessages().add(new LocalizableMessage("stadium.add.trainer", trainer.getName() + " " + trainer.getSurName()));
        }
        

        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        stadiums = stadiumService.findAllStadiums();
        trainers = trainerService.findAllTrainer();
        //return null to let the event handling continue
        return null;
    }

    public StadiumDTO getStadium() {
        return stadium;
    }

    public void setStadium(StadiumDTO stadium) {
        this.stadium = stadium;
    }

    //--- part for deleting a stadium ----

    public Resolution delete() {
        //only id is filled by the form
        stadium = stadiumService.findStadiumById(stadium.getId());
        List<BadgeDTO> badgeDTOList = badgeService.findBadgeByStadium(stadium);
        if (badgeDTOList.isEmpty()) {
            trainers = trainerService.findAllTrainer();
            stadiumService.deleteStadium(stadium);
            getContext().getMessages().add(new LocalizableMessage("stadium.delete.message",escapeHTML(stadium.getCity()),escapeHTML(stadium.getType().toString())));
        } else {
            StringBuilder sb = new StringBuilder();
                sb.append("<ul>");
                for (BadgeDTO badge : badgeDTOList) {
                    sb.append("<li>");
                    sb.append(badge.getStadium() + " - " + badge.getTrainer());
                    sb.append("</li>");                
                }
                sb.append("</ul>");

                getContext().getMessages().add(new LocalizableMessage("stadium.delete.badge", sb.toString()));
        }
        
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for editing a stadium ----

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadStadiumFromDatabase() {
        String ids = getContext().getRequest().getParameter("stadium.id");
        if (ids == null) return;
        stadium = stadiumService.findStadiumById(Long.parseLong(ids));
        trainers = trainerService.findAllTrainer();
    }

    public Resolution edit() {

        return new ForwardResolution("/stadium/edit.jsp");
    }

    public Resolution save() {
        stadium.setTrainer(trainerService.findTrainerById(stadium.getTrainer().getId()));
        stadiumService.updateStadium(stadium);
        return new RedirectResolution(this.getClass(), "list");
    }
}
