package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.AccountDTO;
import cz.muni.cz.api.service.AccountService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.*;

/**
 * Implementation of login security service
 *
 * @author Tomáš Sládek, 359532
 */
public class UserDetailsServiceImpl implements UserDetailsService, Serializable {

    @Autowired
    AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final AccountDTO accountDTO = accountService.findByName(username);
        if (accountDTO == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return new UserDetails() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                GrantedAuthority grantedAuthority = new GrantedAuthority() {
                    @Override
                    public String getAuthority() {
                        return "ROLE_USER";
                    }
                };
                List<GrantedAuthority> listAuthorities = new ArrayList<>();
                listAuthorities.add(grantedAuthority);
                if (accountDTO.getIsAdmin()) {
                    GrantedAuthority adminAuthority = new GrantedAuthority() {
                        @Override
                        public String getAuthority() {
                            return "ROLE_ADMIN";
                        }
                    };
                    listAuthorities.add(adminAuthority);
                }
                return Collections.unmodifiableList(listAuthorities);
            }

            @Override
            public String getPassword() {
                return accountDTO.getPassword();
            }

            @Override
            public String getUsername() {
                return accountDTO.getName();
            }

            @Override
            public boolean isAccountNonExpired() {
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return true;
            }

            @Override
            public boolean isEnabled() {
                return true;
            }
        };
    }

}
