package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.api.service.BadgeService;
import cz.muni.cz.api.service.StadiumService;
import cz.muni.cz.api.service.TrainerService;
import java.util.ArrayList;
import java.util.Iterator;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import java.util.List;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Stripes ActionBean for handling badge operations.
 *
 * @author Tomáš Sládek, 359532
 */
@UrlBinding("/badges/{$event}/{badge.id}")
public class BadgeActionBean extends BaseActionBean implements ValidationErrorHandler {


    @SpringBean //Spring can inject even to private and protected fields
    protected BadgeService badgeService;
    @SpringBean //Spring can inject even to private and protected fields
    protected StadiumService stadiumService;
    @SpringBean
    protected TrainerService trainerService;
    @SpringBean
    protected AccountService accountService;

    //--- part for showing a list of badges ----
    private List<BadgeDTO> badges;
    private List<StadiumDTO> stadiums;
    private List<TrainerDTO> trainers;
    private Long trainerID;

    @DefaultHandler
    public Resolution list() {

        badges = badgeService.findAllBadges();
        
        stadiums = stadiumService.findAllStadiums();
        UserDetails u = (UserDetails) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String name = u.getUsername();
        trainerID = accountService.findByName(name).getTrainerID();
        boolean isAdmin = getContext().getRequest().isUserInRole("ROLE_ADMIN");
        stadiums = stadiumService.findAllStadiums();
        if (!isAdmin) {
            ArrayList toRemove = new ArrayList();
            for (StadiumDTO s : stadiums) {
                if (s.getTrainer().getId() != trainerID) {
                    toRemove.add(s);
                }
            }
            stadiums.removeAll(toRemove);
        }

        trainers = trainerService.findAllTrainer();
        return new ForwardResolution("/badge/list.jsp");
    }

    public List<BadgeDTO> getBadges() {
        return badges;
    }
    
    public List<StadiumDTO> getStadiums() {
        return stadiums;
    }
    
    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    //--- part for adding a badge ----

    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "trainer.id", required = true),
            @Validate(on = {"add", "save"}, field = "stadium.id", required = true),
    })
    private BadgeDTO badge;

    public Resolution add() {
        badge.setStadium(stadiumService.findStadiumById(badge.getStadium().getId()));
        badge.setTrainer(trainerService.findTrainerById(badge.getTrainer().getId()));

        badgeService.createBadge(badge);
        getContext().getMessages().add(new LocalizableMessage("badge.add.message",escapeHTML(badge.getStadium().toString()),escapeHTML(badge.getTrainer().toString())));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        badges = badgeService.findAllBadges();
        stadiums = stadiumService.findAllStadiums();
        trainers = trainerService.findAllTrainer();
        //return null to let the event handling continue
        return null;
    }

    public BadgeDTO getBadge() {
        return badge;
    }

    public void setBadge(BadgeDTO badge) {
        this.badge = badge;
    }

    //--- part for deleting a badge ----

    public Resolution delete() {

        //only id is filled by the form
        badge = badgeService.findBadgeById(badge.getId());
        stadiums = stadiumService.findAllStadiums();
        trainers = trainerService.findAllTrainer();
        badgeService.deleteBadge(badge);
        getContext().getMessages().add(new LocalizableMessage("badge.delete.message",escapeHTML(badge.getStadium().toString()),escapeHTML(badge.getTrainer().toString())));
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for editing a badge ----

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadBadgeFromDatabase() {
        String ids = getContext().getRequest().getParameter("badge.id");
        if (ids == null) return;
        badge = badgeService.findBadgeById(Long.parseLong(ids));
        stadiums = stadiumService.findAllStadiums();
        trainers = trainerService.findAllTrainer();
    }

    public Resolution edit() {

        return new ForwardResolution("/badge/edit.jsp");
    }

    public Resolution save() {

        badgeService.updateBadge(badge);
        return new RedirectResolution(this.getClass(), "list");
    }
}
