package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.api.service.TrainerService;
import java.util.ArrayList;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.controller.LifecycleStage;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;

import java.util.List;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Stripes ActionBean for handling pokemon operations.
 *
 * @author Jan Rychnovský
 */
@UrlBinding("/pokemons/{$event}/{pokemon.id}")
public class PokemonActionBean extends BaseActionBean implements ValidationErrorHandler {



    @SpringBean //Spring can inject even to private and protected fields
    protected PokemonService pokemonService;
    @SpringBean
    protected TrainerService trainerService;
    @SpringBean
    protected AccountService accountService;

    //--- part for showing a list of pokemon ----
    private List<PokemonDTO> pokemons;
    private List<TrainerDTO> trainers;
    private Long trainerID;

    @DefaultHandler
    public Resolution list() {

        UserDetails u = (UserDetails) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        String name = u.getUsername();
        trainerID = accountService.findByName(name).getTrainerID();
        boolean isAdmin = getContext().getRequest().isUserInRole("ROLE_ADMIN");
        if (isAdmin) {
            trainers = trainerService.findAllTrainer();
        } else {
            if (trainers == null) {
                trainers = new ArrayList<>();
            } else {
                trainers.clear();
            }
            TrainerDTO t = trainerService.findTrainerById(trainerID);
            trainers.add(t);
        }

        pokemons = pokemonService.findAllPokemon();

        return new ForwardResolution("/pokemon/list.jsp");
    }

    public Long getTrainerID() {
        return trainerID;
    }
    
    public List<PokemonDTO> getPokemons() {
        return pokemons;
    }
    
     public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    //--- part for adding a pokemon ----

    @ValidateNestedProperties(value = {
            @Validate(on = {"add", "save"}, field = "name", required = true),
            @Validate(on = {"add", "save"}, field = "nick", required = true),
            @Validate(on = {"add", "save"}, field = "type", required = true),
            @Validate(on = {"add", "save"}, field = "level", required = true),
            @Validate(on = {"add", "save"}, field = "trainer.id", required = true)
    })
    private PokemonDTO pokemon;

    public Resolution add() {
        pokemon.setTrainer(trainerService.findTrainerById(pokemon.getTrainer().getId()));

        pokemonService.createPokemon(pokemon);
        getContext().getMessages().add(new LocalizableMessage("pokemon.add.message",escapeHTML(pokemon.getName()),escapeHTML(pokemon.getNick())));
        return new RedirectResolution(this.getClass(), "list");
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
        //fill up the data for the table if validation errors occured
        pokemons = pokemonService.findAllPokemon();
        trainers = trainerService.findAllTrainer();
        //return null to let the event handling continue
        return null;
    }

    public PokemonDTO getPokemon() {
        return pokemon;
    }

    public void setPokemon(PokemonDTO pokemon) {
        this.pokemon = pokemon;
    }

    //--- part for deleting a pokemon ----

    public Resolution delete() {

        //only id is filled by the form
        pokemon = pokemonService.findPokemonById(pokemon.getId());
        trainers = trainerService.findAllTrainer();
        pokemonService.deletePokemon(pokemon);
        getContext().getMessages().add(new LocalizableMessage("pokemon.delete.message",escapeHTML(pokemon.getName()),escapeHTML(pokemon.getNick())));
        return new RedirectResolution(this.getClass(), "list");
    }

    //--- part for editing a pokemon ----

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"edit", "save"})
    public void loadPokemonFromDatabase() {
        String ids = getContext().getRequest().getParameter("pokemon.id");
        if (ids == null) return;
        pokemon = pokemonService.findPokemonById(Long.parseLong(ids));
            trainers = trainerService.findAllTrainer();
    }

    public Resolution edit() {

        return new ForwardResolution("/pokemon/edit.jsp");
    }

    public Resolution save() {      
        pokemon.setTrainer(trainerService.findTrainerById(pokemon.getTrainer().getId()));
        pokemonService.updatePokemon(pokemon);
        return new RedirectResolution(this.getClass(), "list");
    }
}