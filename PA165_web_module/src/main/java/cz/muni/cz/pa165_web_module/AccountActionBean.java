package cz.muni.cz.pa165_web_module;

import cz.muni.cz.api.dto.AccountDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.api.service.TrainerService;
import net.sourceforge.stripes.action.*;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.*;

import java.util.List;
import net.sourceforge.stripes.controller.LifecycleStage;

/**
 * Stripes ActionBean for handling account operations.
 *
 * @author: Tomas Sladek, 359532
 */
@UrlBinding("/accounts/{$event}")
public class AccountActionBean extends BaseActionBean implements ValidationErrorHandler {

    @SpringBean
    private AccountService accountService;
    @SpringBean
    protected TrainerService trainerService;

    private List<AccountDTO> accounts;
    private List<TrainerDTO> trainers;

    @ValidateNestedProperties(
            value = {
                @Validate(on = {"createAccount", "updateAccount"}, field = "name", required = true, maxlength = 50),
                @Validate(on = {"createAccount"}, field = "password", required = true, minlength = 6, maxlength = 50)
            }
    )
    private AccountDTO account;

    public List<TrainerDTO> getTrainers() {
        return trainers;
    }

    public List<AccountDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountDTO> accounts) {
        this.accounts = accounts;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    @DefaultHandler
    public Resolution list() {
        accounts = accountService.findAll();
        trainers = trainerService.findAllTrainer();
        return new ForwardResolution("/account/accList.jsp");
    }

    /**
     * creates account
     *
     * @return
     */
    public Resolution createAccount() {
        // checkbox is unchecked, in stripes it equals 'false'
        // set false explicitly
        if (account.getIsAdmin() == null) {
            account.setIsAdmin(false);
        }

        // resolve case when userName is already used
        AccountDTO usedAccount = accountService.findByName(account.getName());
        if (usedAccount != null) {
            getContext().getMessages().add(new LocalizableError("account.usedusername"));
            return new ForwardResolution(this.getClass(), "list");
        }

        accountService.create(account);
        getContext().getMessages().add(new LocalizableMessage("account.add.message", escapeHTML(account.toString())));
        return new ForwardResolution(this.getClass(), "list");
    }

    public Resolution deleteAccount() {
        accountService.delete(account);
        return new RedirectResolution(this.getClass(), "list");
    }

    /**
     * redirects to account edit page
     *
     * @return
     */
    public Resolution editAccount() {
        return new ForwardResolution("/account/accEdit.jsp");
    }

    public Resolution updateAccount() {
        if (account.getIsAdmin() == null) {
            account.setIsAdmin(false);
        }

        try {
            AccountDTO usedAccount = accountService.findByName(account.getName());
            if (usedAccount != null && account.getId() == usedAccount.getId()) {
                getContext().getMessages().add(new LocalizableError("accounts.usedusername"));
                return new ForwardResolution(this.getClass(), "list");
            }
        } catch (IndexOutOfBoundsException e) {
            getContext().getMessages().add(new LocalizableError("account.usedusername"));
            return new ForwardResolution(this.getClass(), "list");
        }

        accountService.update(account);
        account = null;
        return new RedirectResolution(this.getClass(), "list");
    }

    @Before(stages = LifecycleStage.BindingAndValidation, on = {"editAccount", "updateAccount"})
    public void loadAccountFromDatabase() {
        String ids = getContext().getRequest().getParameter("account.id");
        if (ids == null) {
            return;
        }
        account = accountService.findById(Long.parseLong(ids));
        trainers = trainerService.findAllTrainer();
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors validationErrors) throws Exception {
        accounts = accountService.findAll();
        trainers = trainerService.findAllTrainer();
        return null;
    }
}
