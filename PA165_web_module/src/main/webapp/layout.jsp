<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<s:layout-definition>
<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<head>
  <title><f:message key="${titlekey}"/></title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css" />
  <s:layout-component name="header"/>
</head>
<body>
    <div id="container">
   <h1><f:message key="${titlekey}"/></h1>
   
   <div id="tabs">
       <sec:authorize access="isAuthenticated()">
           <ul>
               <li><s:link beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean"><span><f:message key="navigation.trainer"/></span></s:link></li>
               <li><s:link beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean"><span><f:message key="navigation.pokemon"/></span></s:link></li>
               <li><s:link beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean"><span><f:message key="navigation.stadium"/></span></s:link></li>
               <li><s:link beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean"><span><f:message key="navigation.badge"/></span></s:link></li>
                       <sec:authorize access="hasRole('ROLE_ADMIN')">
                   <li><s:link beanclass="cz.muni.cz.pa165_web_module.AccountActionBean"><span><f:message key="navigation.account"/></span></s:link></li>
                       </sec:authorize>   
           </ul>
           <ul>             
               <li id="line">
                   <button type="submit" class="btn"
                           onclick="location.href='${pageContext.request.contextPath}' + '/j_spring_security_logout';">
                       <f:message key="navigation.logout"/>
                   </button>
               </li>
               <li id="line"><f:message key="navigation.accountName"/><sec:authentication property="principal.username" /></li>
           </ul>
       </sec:authorize>  
   </div>

     <div id="container">
        <s:messages/>
        <s:layout-component name="body"/>
    </div>
    </div>
</body>
<hr />
<div id="pokeball" class="fix"></div>
</html>
</s:layout-definition>