<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>

<s:layout-render name="/layout.jsp" titlekey="account.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.AccountActionBean" var="actionBean"/>

        <p id="message"><f:message key="account.list.allaccounts"/></p>

        <table class="basic">
            <tr>
                <th>ID</th>
                <th><f:message key="account.name"/></th>
                <th><f:message key="account.isAdmin"/></th>
                <th><f:message key="account.trainerID"/></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${actionBean.accounts}" var="account">
                <tr>
                    <td>${account.id}</td>
                    <td><c:out value="${account.name}"/></td>
                    <td><c:out value="${account.isAdmin}"/></td>
                    <td><c:out value="${account.trainerID}"/></td>
                    <td>
                     <s:link beanclass="cz.muni.cz.pa165_web_module.AccountActionBean" event="editAccount"><s:param name="account.id" value="${account.id}"/><f:message key="account.edit.edit"/></s:link>
                    </td>
                    <td>
                        <s:form beanclass="cz.muni.cz.pa165_web_module.AccountActionBean">
                            <s:hidden name="account.id" value="${account.id}"/>
                            <s:submit name="deleteAccount"><f:message key="account.list.delete"/></s:submit>
                        </s:form>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <div id="form">
        <s:form beanclass="cz.muni.cz.pa165_web_module.AccountActionBean">
            <fieldset><legend><p id="legend"><f:message key="account.list.newaccount"/></p></legend>
                <%@include file="form.jsp"%>
                <s:submit name="createAccount"><f:message key="account.create"/></s:submit>
            </fieldset>
        </s:form>
        </div>
    </s:layout-component>
</s:layout-render>