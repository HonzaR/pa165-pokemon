<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="b1" name="account.name"/></th>
        <td><s:text id="b1" name="account.name"/></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="account.password"/></th>
        <td><s:text id="b2" name="account.password"/></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="account.isAdmin"/></th>
       <td><s:checkbox id="b3" name="account.isAdmin"/></td>        
    </tr>
    <tr>
        <th><s:label for="b4" name="account.trainerID"/></th>
        <td><s:select id="b4" name="account.trainerID">
            <s:options-collection collection="${actionBean.trainers}" value="id" label="description"/>
        </s:select></td>
    </tr>
</table>