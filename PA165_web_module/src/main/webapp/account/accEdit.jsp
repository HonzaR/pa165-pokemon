<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:layout-render name="/layout.jsp" titlekey="account.edit.title">
    <s:layout-component name="body">
        <br/><br/><br/><br/>
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.AccountActionBean" var="actionBean"/>

        <s:form beanclass="cz.muni.cz.pa165_web_module.AccountActionBean">
            <s:hidden name="account.id"/>
            <fieldset><legend><f:message key="account.edit.edit"/></legend>
                <s:errors/>
                <table>
                    <tr>
                        <th><s:label for="b1" name="account.name"/></th>
                        <td><s:text id="b1" name="account.name"/></td>
                    </tr>
                    <tr>
                        <th><s:label for="b2" name="account.password"/></th>
                        <td><input id="b2" name="account.password" value=""/></td>
                    </tr>
                    <tr>
                        <th><s:label for="b2" name="account.isAdmin"/></th>
                        <td><s:checkbox id="b3" name="account.isAdmin"/></td>        
                    </tr>
                    <tr>
                        <th><s:label for="b4" name="account.trainerID"/></th>
                        <td><s:select id="b4" name="account.trainerID">
                                <s:options-collection collection="${actionBean.trainers}" value="id" label="description"/>
                            </s:select></td>
                    </tr>
                </table>
                <s:submit name="updateAccount"><f:message key="account.edit.save"/></s:submit>
                </fieldset>
        </s:form>

    </s:layout-component>
</s:layout-render>