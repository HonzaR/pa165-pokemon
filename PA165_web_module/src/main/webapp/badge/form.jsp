<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="b1" name="badge.trainer"/></th>
        <td><s:select id="b1" name="badge.trainer.id">
            <s:options-collection collection="${actionBean.trainers}" value="id" label="description"/>
        </s:select></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="badge.stadium"/></th>
        <td><s:select id="b2" name="badge.stadium.id">
            <s:options-collection collection="${actionBean.stadiums}" value="id" label="description"/>
        </s:select></td>
    </tr>
</table>