<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:layout-render name="/layout.jsp" titlekey="badge.edit.title">
    <s:layout-component name="body">
        <br/><br/><br/><br/>
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean" var="actionBean"/>

        <s:form beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean">
            <s:hidden name="badge.id"/>
            <fieldset><legend><f:message key="badge.edit.edit"/></legend>
                <%@include file="form.jsp"%>
                <s:submit name="save"><f:message key="badge.edit.save"/></s:submit>
            </fieldset>
        </s:form>

    </s:layout-component>
</s:layout-render>