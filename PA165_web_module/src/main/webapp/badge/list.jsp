<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:layout-render name="/layout.jsp" titlekey="badge.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean" var="actionBean"/>

        <p id="message"><f:message key="badge.list.allbadges"/></p>

        <table class="basic">
            <tr>
                <th>ID</th>
                <th><f:message key="badge.trainer"/></th>
                <th><f:message key="badge.city"/></th>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <th></th>
                <th></th>
                </sec:authorize>
            </tr>
            <c:forEach items="${actionBean.badges}" var="badge">
                <tr>
                    <td>${badge.id}</td>
                    <td><c:out value="${badge.trainer}"/></td>
                    <td><c:out value="${badge.stadium}"/></td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                    <td>
                     <s:link beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean" event="edit"><s:param name="badge.id" value="${badge.id}"/><f:message key="badge.edit.edit"/></s:link>
                    </td>
                    <td>
                        <s:form beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean">
                            <s:hidden name="badge.id" value="${badge.id}"/>
                            <s:submit name="delete"><f:message key="badge.list.delete"/></s:submit>
                        </s:form>        
                    </td>
                    </sec:authorize>
                </tr>
            </c:forEach>
        </table>

        <div id="form">
        <s:form beanclass="cz.muni.cz.pa165_web_module.BadgeActionBean">
            <fieldset><legend><p id="legend"><f:message key="badge.list.newbadge"/></p></legend>
                <%@include file="form.jsp"%>
                <s:submit name="add"><f:message key="badge.create"/></s:submit>
            </fieldset>
        </s:form>
        </div>
    </s:layout-component>
</s:layout-render>