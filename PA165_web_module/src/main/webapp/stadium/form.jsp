<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="s3" name="stadium.city"/></th>
        <td><s:text id="s3" name="stadium.city"/></td>
    </tr>
    <tr>
        <th><s:label for="s2" name="stadium.type"/></th>
        <td><s:select id="s2" name="stadium.type"><s:options-enumeration enum="property.Type"/></s:select></td>
    </tr>
    <tr>
        <th><s:label for="s3" name="stadium.trainer"/></th>
        <td><s:select id="s3" name="stadium.trainer.id">
            <s:options-collection collection="${actionBean.trainers}" value="id" label="description"/>
        </s:select></td>
    </tr>
</table>