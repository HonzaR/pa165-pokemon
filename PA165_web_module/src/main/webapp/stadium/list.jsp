<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:layout-render name="/layout.jsp" titlekey="stadium.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean" var="actionBean"/>

        <p id="message"><f:message key="stadium.list.allstadiums"/></p>

        <table class="basic">
            <tr>
                <th>id</th>
                <th><f:message key="stadium.city"/></th>
                <th><f:message key="stadium.type"/></th>
                <th><f:message key="stadium.trainer"/></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${actionBean.stadiums}" var="stadium">
                <tr>
                    <td>${stadium.id}</td>
                    <td><c:out value="${stadium.city}"/></td>
                    <td><c:out value="${stadium.type}"/></td>
                    <td><c:out value="${stadium.trainer}"/></td>
                    <c:if test="${stadium.trainer.id == actionBean.trainerID}">
                        <td class="edit">
                            <s:link beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean" event="edit"><s:param name="stadium.id" value="${stadium.id}"/><f:message key="stadium.edit.edit"/></s:link>
                            </td>
                            <td>
                            <s:form beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean">
                                <s:hidden name="stadium.id" value="${stadium.id}"/>
                                <s:submit name="delete"><f:message key="stadium.list.delete"/></s:submit>
                            </s:form>
                        </td>
                    </c:if>
                    <c:if test="${stadium.trainer.id != actionBean.trainerID}">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td class="edit">
                                <s:link beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean" event="edit"><s:param name="stadium.id" value="${stadium.id}"/><f:message key="stadium.edit.edit"/></s:link>
                                </td>
                                <td>
                                <s:form beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean">
                                    <s:hidden name="stadium.id" value="${stadium.id}"/>
                                    <s:submit name="delete"><f:message key="stadium.list.delete"/></s:submit>
                                </s:form>
                            </td>
                        </sec:authorize>
                    </c:if>
                </tr>
            </c:forEach>
        </table>

        <div id="form">
            <s:form beanclass="cz.muni.cz.pa165_web_module.StadiumActionBean">
                <fieldset><legend><p id="legend"><f:message key="stadium.list.createstadium"/></p></legend>
                <%@include file="form.jsp"%>
                <s:submit name="add"><f:message key="stadium.create"/></s:submit>
            </fieldset>
        </s:form>
        </div>
    </s:layout-component>
</s:layout-render>