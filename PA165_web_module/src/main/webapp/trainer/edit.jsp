<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:layout-render name="/layout.jsp" titlekey="trainer.edit.title">
    <s:layout-component name="body">
        <br/><br/><br/><br/>
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean" var="actionBean"/>

        <s:form beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean">
            <s:hidden name="trainer.id"/>
            <fieldset><legend><f:message key="trainer.edit.edit"/></legend>
                <%@include file="form.jsp"%>
                <s:submit name="save"><f:message key="trainer.edit.save"/></s:submit>
            </fieldset>
        </s:form>

    </s:layout-component>
</s:layout-render>