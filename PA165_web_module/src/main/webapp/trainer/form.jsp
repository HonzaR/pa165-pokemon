<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="b1" name="trainer.name"/></th>
        <td><s:text id="b1" name="trainer.name"/></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="trainer.surName"/></th>
        <td><s:text id="b2" name="trainer.surName"/></td>
    </tr>
    <tr>
        <th><s:label for="b3" name="trainer.birthDate"/></th>
        <td><s:text id="b3" name="trainer.birthDate"/></td>
        <td><s:label name="trainer.birthDateHint"/></td>
    </tr>
</table>