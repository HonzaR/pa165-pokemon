<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:layout-render name="/layout.jsp" titlekey="trainer.list.title">

    <s:layout-component name="body">

        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean" var="actionBean"/>

        <p id="message"><f:message key="trainer.list.alltrainers"/></p>

        <table class="basic">
            <tr>
                <th>id</th>
                <th><f:message key="trainer.name"/></th>
                <th><f:message key="trainer.surName"/></th>
                <th><f:message key="trainer.birthDate"/></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${actionBean.trainers}" var="trainer">
                <tr>
                    <td>${trainer.id}</td>
                    <td><c:out value="${trainer.name}"/></td>
                    <td><c:out value="${trainer.surName}"/></td>
                    <td><c:out value="${trainer.birthDate}"/></td>
                    <c:if test="${trainer.id == actionBean.trainerID}">
                        <td>
                            <s:link beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean" event="edit"><s:param name="trainer.id" value="${trainer.id}"/><f:message key="trainer.edit.edit"/></s:link>
                            </td>
                            <td>
                            <s:form beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean">
                                <s:hidden name="trainer.id" value="${trainer.id}"/>
                                <s:submit name="delete"><f:message key="trainer.list.delete"/></s:submit>
                            </s:form>
                        </td>
                    </c:if>
                    <c:if test="${trainer.id != actionBean.trainerID}">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td>
                                <s:link beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean" event="edit"><s:param name="trainer.id" value="${trainer.id}"/><f:message key="trainer.edit.edit"/></s:link>
                                </td>
                                <td>
                                <s:form beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean">
                                    <s:hidden name="trainer.id" value="${trainer.id}"/>
                                    <s:submit name="delete"><f:message key="trainer.list.delete"/></s:submit>
                                </s:form>
                            </td>
                        </sec:authorize>
                    </c:if> 
                </tr>
            </c:forEach>
        </table>
        <sec:authorize access="hasRole('ROLE_ADMIN')">            
            <div id="form">
                <s:form beanclass="cz.muni.cz.pa165_web_module.TrainerActionBean">
                    <fieldset><legend><p id="legend"><f:message key="trainer.list.newtrainer"/></p></legend>
                            <%@include file="form.jsp"%>
                            <s:submit name="add"><f:message key="trainer.create"/></s:submit>
                        </fieldset>
                </s:form>
            </div>
        </sec:authorize>
    </s:layout-component>
</s:layout-render>