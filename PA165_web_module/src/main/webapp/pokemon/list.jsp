<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<s:layout-render name="/layout.jsp" titlekey="pokemon.list.title">
    <s:layout-component name="body">
        <s:useActionBean beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean" var="actionBean"/>

        <p id="message"><f:message key="pokemon.list.allpokemons"/></p>

        <table class="basic">
            <tr>
                <th>ID</th>
                <th><f:message key="pokemon.name"/></th>
                <th><f:message key="pokemon.nick"/></th>
                <th><f:message key="pokemon.type"/></th>
                <th><f:message key="pokemon.level"/></th>
                <th><f:message key="pokemon.trainer"/></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${actionBean.pokemons}" var="pokemon">
                <tr>
                    <td>${pokemon.id}</td>
                    <td><c:out value="${pokemon.name}"/></td>
                    <td><c:out value="${pokemon.nick}"/></td>
                    <td><c:out value="${pokemon.type}"/></td>
                    <td><c:out value="${pokemon.level}"/></td>
                    <td><c:out value="${pokemon.trainer}"/></td>
                    <c:if test="${pokemon.trainer.id == actionBean.trainerID}">
                    <td>
                    <s:link beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean" event="edit"><s:param name="pokemon.id" value="${pokemon.id}"/><f:message key="pokemon.edit.edit"/></s:link>
                    </td>
                    <td>
                    <s:form beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean">
                        <s:hidden name="pokemon.id" value="${pokemon.id}"/>
                        <s:submit name="delete"><f:message key="pokemon.list.delete"/></s:submit>
                    </s:form>
                    </td>
                    </c:if>
                    <c:if test="${pokemon.trainer.id != actionBean.trainerID}">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td>
                                <s:link beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean" event="edit"><s:param name="pokemon.id" value="${pokemon.id}"/><f:message key="pokemon.edit.edit"/></s:link>
                                </td>
                                <td>
                                <s:form beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean">
                                    <s:hidden name="pokemon.id" value="${pokemon.id}"/>
                                    <s:submit name="delete"><f:message key="pokemon.list.delete"/></s:submit>
                                </s:form>
                            </td>
                        </sec:authorize>
                    </c:if>

                    
                </tr>
            </c:forEach>
        </table>

        <div id="form">
        <s:form beanclass="cz.muni.cz.pa165_web_module.PokemonActionBean">
            <fieldset><legend><p id="legend"><f:message key="pokemon.list.newpokemon"/></p></legend>
                <%@include file="form.jsp"%>
                <s:submit name="add"><f:message key="pokemon.create"/></s:submit>
            </fieldset>
        </s:form><div id="form">
        </div>
    </s:layout-component>
</s:layout-render>