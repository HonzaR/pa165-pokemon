<%@ taglib prefix="s" uri="http://stripes.sourceforge.net/stripes.tld" %>
<s:errors/>
<table>
    <tr>
        <th><s:label for="b1" name="pokemon.name"/></th>
        <td><s:text id="b1" name="pokemon.name"/></td>
    </tr>
    <tr>
        <th><s:label for="b2" name="pokemon.nick"/></th>
        <td><s:text id="b2" name="pokemon.nick"/></td>
    </tr>
    <tr>
        <th><s:label for="b3" name="pokemon.type"/></th>
        <td><s:select id="b3" name="pokemon.type"><s:options-enumeration enum="property.Type"/></s:select></td>
    </tr>
    <tr>
        <th><s:label for="b3" name="pokemon.level"/></th>
        <td><s:text id="b3" name="pokemon.level"/></td>
    </tr>
    <tr>
        <th><s:label for="b4" name="pokemon.trainer"/></th>
        <td><s:select id="b4" name="pokemon.trainer.id">
            <s:options-collection collection="${actionBean.trainers}" value="id" label="description"/>
        </s:select></td>
    </tr>    
</table>