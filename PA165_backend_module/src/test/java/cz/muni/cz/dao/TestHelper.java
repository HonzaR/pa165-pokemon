/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.dao;

import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.Date;
import property.Type;

/**
 *
 * @author Tomas Sladek, 359532
 */
public class TestHelper {
    
    public static Trainer createTrainer(Trainer trainer) {
        trainer.setBirthDate(new Date(1900,1,1));
        trainer.setName("Jarek");
        trainer.setName("Parek");
        return trainer;
    }
    
    public static Stadium createStadium(Stadium stadium) {
        stadium.setCity("Pepikov");
        stadium.setType(Type.GHOST);
        return stadium;
    }
    
    
}
