package cz.muni.cz.dao;

import cz.muni.cz.backend.dao.BadgeDAO;
import cz.muni.cz.backend.dao.StadiumDAO;
import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.dao.impl.BadgeDAOImpl;
import cz.muni.cz.backend.dao.impl.StadiumDAOImpl;
import cz.muni.cz.backend.dao.impl.TrainerDAOImpl;
import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import property.Type;


/**
 *
 * @author Tomáš Sekera, 359902
 */
public class BadgeDAOImplTest extends TestCase{
    
    
    private EntityManager em;
    private BadgeDAO dao;
    private StadiumDAO stadDao;
    private TrainerDAO trainDao;
    
    public BadgeDAOImplTest()
    {
        super("Badge DAO implementation test");
    }
    
    @Override
    protected void setUp() throws java.lang.Exception
    {
        super.setUp();
        
        EntityManagerFactory emFact = Persistence.createEntityManagerFactory("TestPU");
        em = emFact.createEntityManager();
        
        dao = new BadgeDAOImpl();
        stadDao = new  StadiumDAOImpl();
        stadDao.setManagerContext(em);
        dao.setManagerContext(em);
        trainDao = new TrainerDAOImpl();
        trainDao.setManagerContext(em);

      
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        em.close();
    }      
    
    public void testCreateBadge()
    {
         
        try
        {
            dao.createBadge(null);
            fail("Badge with null");
        }
        catch(Exception e)
        {}
        
        Badge badge1 = new Badge();
        Badge badge2;
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
       
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
       
        stad1.setCity("brno");
        stad1.setType(Type.BUG);
        stad1.setTrainer(train1);
       
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        em.getTransaction().commit();
        
        Long id = badge1.getId();
        badge2 = dao.findBadgeById(id);
        
        assertEquals(badge1.getId(), badge2.getId());
        assertEquals(badge1.getStadium(), badge2.getStadium());
        assertEquals(badge1.getTrainer(), badge2.getTrainer());
        
        em.remove(train1);
        em.remove(stad1);
        em.remove(badge1);
 
        
    }
    
    public void testUpdateBadge()
    {
        //null test
        try
        {
            dao.updateBadge(null);
            fail("Badge with null");
        }
        catch(Exception e)
        {}
        
        //creating badge
        Badge badge1 = new Badge();
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
        Trainer train2 = new Trainer();
        Stadium stad2 = new Stadium();
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
              
        stad1.setCity("brno");
        stad1.setType(Type.DARK);
        stad1.setTrainer(train1);
        
              
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        em.getTransaction().commit();
        

        
        train2.setName("Franta");
        train2.setSurName("Trolo");
        train2.setBirthDate(new Date());
        
        stad2.setCity("Praha");
        stad2.setType(Type.DARK);
        stad2.setTrainer(train2);

        badge1.setStadium(stad2);
        badge1.setTrainer(train2);
        
        //update
        em.getTransaction().begin();
        trainDao.createTrainer(train2);
        stadDao.createStadium(stad2);
        dao.updateBadge(badge1);
        em.getTransaction().commit();
        
        assertEquals(badge1.getStadium(), stad2);
        assertEquals(badge1.getTrainer(), train2);
        
        em.remove(train1);
        em.remove(stad1);
        em.remove(badge1);
        em.remove(train2);
        em.remove(stad2);
        
        
    }
    
    public void testDeleteBadge()
    {
        //null test
        try
        {
            dao.deleteBadge(null);
            fail("Badge with null");
        }
        catch(Exception e)
        {}        
        
        //creating badge
        Badge badge1 = new Badge();
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
        
              
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
        
        stad1.setCity("brno");
        stad1.setType(Type.ELECTRIC);
        stad1.setTrainer(train1);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        em.getTransaction().commit();
        
        assertNotNull(badge1);
        
        em.getTransaction().begin();
        dao.deleteBadge(badge1);
        em.getTransaction().commit();
        
        List<Badge> result = dao.findAllBadges();
        
        //should be empty now
        assertTrue(result.isEmpty());
        
        em.remove(train1);
        em.remove(stad1);
             
    }
    
    public void testFindById()
    {
        //null test
        try
        {
            dao.findBadgeById(null);
            fail("Badge with null");
        }
        catch(Exception e)
        {} 
        
        //creating badge
        Badge badge1 = new Badge();
        Badge badge2 = badge1;
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
        
        stad1.setCity("brno");
        stad1.setType(Type.FIRE);
        stad1.setTrainer(train1);
              
        
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        em.getTransaction().commit();
        
        assertNotNull(badge1);
        
        badge1  = dao.findBadgeById(badge1.getId());
        assertEquals(badge1, badge2);
        
        em.remove(train1);
        em.remove(stad1);
        em.remove(badge1);
    }
    
    public void testFindAllBadges()
    {
        //creating badge
        Badge badge1 = new Badge();
        Badge badge2 = new Badge();
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
        
        stad1.setCity("brno");
        stad1.setType(Type.GRASS);
        stad1.setTrainer(train1);
        
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        badge2.setStadium(stad1);
        badge2.setTrainer(train1);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        dao.createBadge(badge2);
        em.getTransaction().commit();
        
        List<Badge> list = dao.findAllBadges();
        assertTrue(list.contains(badge1));
        assertTrue(list.contains(badge2));
        assertTrue(list.size() == 2);
        
        em.remove(train1);
        em.remove(stad1);
        em.remove(badge1);
        em.remove(badge2);
                    
    }
    
    public void testFindBadgeByStadium()
    {
        //null test
        try
        {
            dao.findBadgeByStadium(null);
            fail("Stadium with null");
        }
        catch(Exception e)
        {} 
        
        //creating badge
        Badge badge1 = new Badge();
        Badge badge2 = new Badge();
        Stadium stad1 = new Stadium();
        Trainer train1 = new Trainer();
        Trainer train2 = new Trainer();
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
        
        train2.setName("Franta");
        train2.setSurName("Trolo");
        train2.setBirthDate(new Date());
        
        stad1.setCity("brno");
        stad1.setType(Type.GRASS);
        stad1.setTrainer(train1);
        
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        badge2.setStadium(stad1);
        badge2.setTrainer(train2);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        trainDao.createTrainer(train2);
        stadDao.createStadium(stad1);
        dao.createBadge(badge1);
        dao.createBadge(badge2);
        em.getTransaction().commit();
        
        List<Badge> list = dao.findBadgeByStadium(stad1);
        
        assertEquals(list.get(0), badge1);
        assertTrue(list.size() == 2);
        
        
        
    }
    
    public void testFindBadgeByTrainer()
    {
        
        //null test
        try
        {
            dao.findBadgeByTrainer(null);
            fail("Stadium with null");
        }
        catch(Exception e)
        {} 
        
        //creating badge
        Badge badge1 = new Badge();
        Badge badge2 = new Badge();
        Stadium stad1 = new Stadium();
        Stadium stad2 = new Stadium();
        Trainer train1 = new Trainer();
        
        train1.setName("Pepa");
        train1.setSurName("Trolo");
        train1.setBirthDate(new Date());
        
        
        stad1.setCity("brno");
        stad1.setType(Type.FLYING);
        stad1.setTrainer(train1);
    
        stad2.setCity("Praha");
        stad2.setType(Type.GROUND);
        stad2.setTrainer(train1);
        
        
        badge1.setStadium(stad1);
        badge1.setTrainer(train1);
        
        badge2.setStadium(stad2);
        badge2.setTrainer(train1);
        
        em.getTransaction().begin();
        trainDao.createTrainer(train1);
        stadDao.createStadium(stad1);
        stadDao.createStadium(stad2);
        dao.createBadge(badge1);
        dao.createBadge(badge2);
        em.getTransaction().commit();
        
        List<Badge> list = dao.findBadgeByTrainer(train1);
        
        assertTrue(list.contains(badge1));
        assertTrue(list.contains(badge2));
        assertTrue(list.size() == 2);
        
        
    }
}
