/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.dao;

import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.dao.impl.TrainerDAOImpl;
import cz.muni.cz.backend.entities.Trainer;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author Martin Prokop
 */
public class TrainerDAOImplTest extends TestCase{
    
    private TrainerDAO trainerDAO;
    private EntityManager manager;

    public TrainerDAOImplTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        manager = emf.createEntityManager();
        trainerDAO = new TrainerDAOImpl();
        trainerDAO.setManagerContext(manager);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        manager.close();
    }  
    
    @Test
    public void testCreateTrainer() {

        try {
            trainerDAO.createTrainer(null);
            fail("creating NULL trainer");
        } catch (IllegalArgumentException ex) {

        }

        manager.getTransaction().begin();
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        trainerDAO.createTrainer(trainer);
        manager.getTransaction().commit();

        Long id = trainer.getId();
        Trainer trainer2 = trainerDAO.findTrainerById(id);

        assertEquals(trainer.getId(), trainer2.getId());
        assertEquals(trainer.getName(), trainer2.getName());
        assertEquals(trainer.getSurName(), trainer2.getSurName());
        assertEquals(trainer.getBirthDate(), trainer2.getBirthDate());
        
        manager.remove(trainer);
    }
    
    public void testUpdateTrainer() {

        try {
            trainerDAO.updateTrainer(null);
            fail("updating NULL trainer");
        } catch (IllegalArgumentException ex) {

        }

        manager.getTransaction().begin();
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        trainerDAO.createTrainer(trainer);
        manager.getTransaction().commit();

        manager.getTransaction().begin();
        trainer.setName("Janek");
        trainer.setSurName("Danek");
        trainer.setBirthDate(new Date(1990, 4, 30));
        trainerDAO.updateTrainer(trainer);
        manager.getTransaction().commit();

        assertNotNull(trainer.getId());
        assertEquals(trainer.getName(), "Janek");
        assertEquals(trainer.getSurName(), "Danek");
        assertEquals(trainer.getBirthDate(), new Date(1990, 4, 30));
    }
    
    public void testDeleteTrainer() {

        try {
            trainerDAO.deleteTrainer(null);
            fail("removing NULL trainer");
        } catch (IllegalArgumentException ex) {
        }

        manager.getTransaction().begin();
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        trainerDAO.createTrainer(trainer);
        manager.getTransaction().commit();
        assertNotNull(trainer);
        manager.getTransaction().begin();
        trainerDAO.deleteTrainer(trainer);
        manager.getTransaction().commit();

        List<Trainer> result = trainerDAO.findAllTrainer();

        assertTrue(result.isEmpty());
    }
    
    public void testFindTrainerById() {
        
        try {
            trainerDAO.findTrainerById(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }


        Trainer trainer = new Trainer();
        Trainer trainer2 = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        manager.getTransaction().begin();
        trainerDAO.createTrainer(trainer);
        manager.getTransaction().commit();
        
        manager.getTransaction().begin();
        trainer2 = trainerDAO.findTrainerById(trainer.getId());
        manager.getTransaction().commit();

        assertEquals(trainer2.getName(), "Pepa");
        assertEquals(trainer2.getSurName(), "Zdepa");
        assertEquals(trainer2.getBirthDate(), new Date(1990, 4, 25));
    }
    
    public void testFindAllTrainer() {

        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        
        Trainer trainer2 = new Trainer();
        trainer2.setName("Janek");
        trainer2.setSurName("Danek");
        trainer2.setBirthDate(new Date(1990, 4, 30));
        
        manager.getTransaction().begin();
        trainerDAO.createTrainer(trainer);
        trainerDAO.createTrainer(trainer2);
        manager.getTransaction().commit();

        List<Trainer> accountList = trainerDAO.findAllTrainer();
        assertTrue(accountList.size() == 2);
        assertTrue(accountList.contains(trainer));
        assertTrue(accountList.contains(trainer2));
    }
    
    public void testFindTrainerByName() {

        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));
        
        manager.getTransaction().begin();
        trainerDAO.createTrainer(trainer);
        manager.getTransaction().commit();
        trainer = trainerDAO.findTrainerByName(trainer.getName());

        assertEquals(trainer.getName(), "Pepa");
        assertEquals(trainer.getSurName(), "Zdepa");
        assertEquals(trainer.getBirthDate(), new Date(1990, 4, 25));
    }
}
