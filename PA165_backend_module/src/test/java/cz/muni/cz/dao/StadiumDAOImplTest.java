package cz.muni.cz.dao;

import cz.muni.cz.backend.dao.StadiumDAO;
import cz.muni.cz.backend.dao.impl.StadiumDAOImpl;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import junit.framework.TestCase;
import property.Type;


/**
 *
 * @author Tomas Sladek, 359532
 */
public class StadiumDAOImplTest extends TestCase {
    
    private static EntityManager em;
    private static StadiumDAO sdao;
    
 
    
    public StadiumDAOImplTest()
    {
        super("Stadium DAO implementation test");
    }
    
    @Override
    protected void setUp() throws java.lang.Exception
    {
        super.setUp();

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        em = emf.createEntityManager();
        sdao = new StadiumDAOImpl();
        sdao.setManagerContext(em);
      
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        em.close();
    }  
    
    public void testCreateStadium(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium = TestHelper.createStadium(stadium);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer); 
        em.persist(trainer);
        sdao.createStadium(stadium);
        em.getTransaction().commit();
        
        assertNotNull(sdao.findAllStadiums().size());
        assertEquals(sdao.findAllStadiums().size(),1);
        
        em.remove(stadium);
    }
    
    
    public void testUpdateStadium(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium = TestHelper.createStadium(stadium);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        stadium.setCity("city2");
        sdao.updateStadium(stadium);
        em.getTransaction().commit();
        
        Stadium result = em.find(Stadium.class, stadium.getId());
        assertNotNull(result.getId());
        assertTrue(result.getCity().equalsIgnoreCase("city2"));
        
        em.remove(result);
        em.remove(trainer);
    }
    
    
    public void testDeleteStadium(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium = TestHelper.createStadium(stadium);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        
        sdao.deleteStadium(stadium);
        
        Stadium result = em.find(Stadium.class, stadium.getId());
        
        em.getTransaction().commit();
        assertNull(result);      
    }
    
   
    public void testFindAllStadiums(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium.setCity("city");
        stadium.setType(Type.BUG);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        
        Stadium stadium2 = new Stadium();
        stadium2.setCity("city2");
        stadium2.setType(Type.FIGHTING);
        stadium2.setTrainer(trainer);
        em.persist(stadium2);
        
        Stadium stadium3 = new Stadium();
        stadium3.setCity("city3");
        stadium3.setType(Type.DARK);
        stadium3.setTrainer(trainer);
        em.persist(stadium3);
        
        List<Stadium> stadiums = sdao.findAllStadiums();
        
        em.getTransaction().commit();
        assertEquals(stadiums.size(),3);    
    }
    
    
    public void testFindById(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium.setCity("city");
        stadium.setType(Type.FIGHTING);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        
        Stadium stadium2 = new Stadium();
        stadium2.setCity("city2");
        stadium2.setType(Type.FIRE);
        stadium2.setTrainer(trainer);
        em.persist(stadium2);
        
        Stadium result = sdao.findStadiumById(stadium2.getId());
        
        assertNotNull(result); 
        assertEquals(result.getCity(),stadium2.getCity());
        em.getTransaction().commit();
    }
    
    
    public void testFindStadiumByCity(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium.setCity("city");
        stadium.setType(Type.FLYING);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        
        Stadium stadium2 = new Stadium();
        stadium2.setCity("city2");
        stadium2.setType(Type.DRAGON);
        stadium2.setTrainer(trainer);
        em.persist(stadium2);
        
        List<Stadium> result = sdao.findStadiumByCity("city2");

        assertNotNull(result.size()); 
        assertEquals(result.get(0).getCity(),"city2");
        em.getTransaction().commit();
    }
    
    
    public void testFindStadiumByType(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium.setCity("city");
        stadium.setType(Type.FIGHTING);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        em.persist(stadium);
        
        Stadium stadium2 = new Stadium();
        stadium2.setCity("city2");
        stadium2.setType(Type.GHOST);
        stadium2.setTrainer(trainer);
        em.persist(stadium2);
        
        Stadium stadium3 = new Stadium();
        stadium3.setCity("city2");
        stadium3.setType(Type.GHOST);
        stadium3.setTrainer(trainer);
        em.persist(stadium3);
        
        List<Stadium> results = sdao.findStadiumByType(Type.GHOST);
        
        assertEquals(results.size(),2); 
        assertEquals(results.get(0).getType(),Type.GHOST);
        em.getTransaction().commit();
    }
    
  
    public void testFindStadiumByTrainer(){
        em.getTransaction().begin();
        Stadium stadium = new Stadium();
        stadium = TestHelper.createStadium(stadium);
        Trainer trainer = new Trainer();
        trainer = TestHelper.createTrainer(trainer);
        stadium.setTrainer(trainer);
        em.persist(trainer);
        stadium.setTrainer(trainer);
        em.persist(stadium);
        
        Stadium stadium2 = new Stadium();
        stadium2 = TestHelper.createStadium(stadium2);
        stadium2.setCity("Balikov");
        Trainer trainer2 = new Trainer();
        trainer2 = TestHelper.createTrainer(trainer2);
        stadium2.setTrainer(trainer2);
        trainer2.setName("Marek");
        em.persist(trainer2);
        stadium2.setTrainer(trainer2);
        em.persist(stadium2);
        
        Stadium stadium3 = new Stadium();
        stadium3 = TestHelper.createStadium(stadium3);
        stadium3.setCity("Balikov");
        Trainer trainer3 = new Trainer();
        trainer3 = TestHelper.createTrainer(trainer3);
        stadium3.setTrainer(trainer3);
        trainer3.setName("Tarek");
        em.persist(trainer3);
        stadium3.setTrainer(trainer3);
        em.persist(stadium3);
                
        Stadium result = sdao.findStadiumByTrainer(trainer2);
        
        assertNotNull(result); 
        assertEquals(result.getTrainer().getId(),trainer2.getId());
        em.getTransaction().commit();
    }
}
