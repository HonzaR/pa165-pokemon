/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.dao;

import cz.muni.cz.backend.dao.PokemonDAO;
import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.dao.impl.PokemonDAOImpl;
import cz.muni.cz.backend.dao.impl.TrainerDAOImpl;
import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import junit.framework.TestCase;
import property.Type;
//import static junit.framework.TestCase.fail;

/**
 *
 * @author Jan Rychnovský
 */

public class PokemonDAOImplTest extends TestCase {
    
    private EntityManager em;
    private PokemonDAO pdao;
    private TrainerDAO tdao;
    


    public PokemonDAOImplTest(String testName) {
        super(testName);
    }
        
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TestPU");
        em = emf.createEntityManager();
        
        pdao = new PokemonDAOImpl();
        tdao = new TrainerDAOImpl();
        pdao.setManagerContext(em);
        tdao.setManagerContext(em);


    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        em.close();
    }    
    
    @Test
    public void testCreatePokemon(){
        
        
        try {
            pdao.createPokemon(null);
            fail("creating NULL pokemon");
        } catch (IllegalArgumentException ex) {}        
        
        
        Pokemon pokemon = new Pokemon();      
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.ELECTRIC);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));        
        pokemon.setTrainer(trainer);        

        em.getTransaction().begin();
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit();
        
        Long id = trainer.getId();
        Pokemon pokemon2 = pdao.findPokemonById(id);

        assertNotNull(pokemon2.getId());
        assertEquals(pokemon2.getId(),pokemon.getId());
        assertEquals(pokemon2.getName(),pokemon.getName());
        assertEquals(pokemon2.getNick(),pokemon.getNick());
        assertEquals(pokemon2.getLevel(),pokemon.getLevel());
        assertEquals(pokemon2.getType(),pokemon.getType());
        assertEquals(pokemon2.getTrainer(),pokemon.getTrainer());
        
        em.remove(pokemon);
        em.remove(trainer);
    }
    
    public void testUpdatePokemon()
    {
        
        try {
            pdao.updatePokemon(null);
            fail("Badge with null");
        }
        catch(Exception e) {}
        
        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.ELECTRIC);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));            
        pokemon.setTrainer(trainer);         
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit();

        em.getTransaction().begin();      
        pokemon.setName("name2");
        pokemon.setNick("nick2");        
        pokemon.setType(Type.DARK);
        pokemon.setLevel(2);                 
        pdao.updatePokemon(pokemon);
        em.getTransaction().commit();

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name2");
        assertEquals(pokemon.getType(), Type.DARK);
        assertEquals(pokemon.getLevel(), 2);
        assertEquals(pokemon.getTrainer(), trainer);  
        
        em.remove(pokemon);
        em.remove(trainer);
        
    }  
    
    public void testDeletePokemon() {
        

        try {
            pdao.deletePokemon(null);
            fail("removing NULL pokemon");
        } catch (IllegalArgumentException ex) {
        }
        
        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.BUG);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));  
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        
        assertNotNull(trainer);
        
        em.getTransaction().begin();
        pdao.deletePokemon(pokemon);
        em.getTransaction().commit();

        List<Pokemon> result = pdao.findAllPokemon();

        assertTrue(result.isEmpty());


    }  
    
    public void testFindPokemonById() {
        
        try {
            pdao.findPokemonById(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }
        
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FAIRY);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));        
        pokemon.setTrainer(trainer);    
        em.getTransaction().begin();
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        assertNotNull(pokemon.getId());
        
        em.getTransaction().begin();
        pokemon = pdao.findPokemonById(pokemon.getId());
        em.getTransaction().commit(); 
        
        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FAIRY);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    }  
    
    public void testFindPokemonByName() {
        
        try {
            pdao.findPokemonByName(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));         
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        
        assertNotNull(trainer.getId());
        pokemon = pdao.findPokemonByName(pokemon.getName()).get(0);

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FIGHTING);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    } 
    
    public void testFindPokemonByNick() {
        
        try {
            pdao.findPokemonByNick(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));          
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit();
        
        assertNotNull(trainer.getId());
        pokemon = pdao.findPokemonByNick(pokemon.getNick());

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FIGHTING);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    } 
    
    public void testFindPokemonByType() {
        
        try {
            pdao.findPokemonByType(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));         
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        
        assertNotNull(trainer.getId());
        pokemon = pdao.findPokemonByType(pokemon.getType()).get(0);

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FIGHTING);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    }  
    
    public void testFindPokemonByLevel() {
        
        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));          
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        
        assertNotNull(trainer.getId());
        pokemon = pdao.findPokemonByLevel(pokemon.getLevel()).get(0);

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FIGHTING);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    } 
    
    public void testFindPokemonByTrainer() {
        
        try {
            pdao.findPokemonByTrainer(null);
            fail();
        } catch (IllegalArgumentException ex) {
        }

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));         
        pokemon.setTrainer(trainer);     
        pdao.createPokemon(pokemon);
        tdao.createTrainer(trainer);
        em.getTransaction().commit(); 
        
        assertNotNull(trainer.getId());
        pokemon = pdao.findPokemonByTrainer(pokemon.getTrainer()).get(0);

        assertNotNull(pokemon.getId());
        assertEquals(pokemon.getName(), "name1");
        assertEquals(pokemon.getType(), Type.FIGHTING);
        assertEquals(pokemon.getLevel(), 1);
        assertEquals(pokemon.getTrainer(), trainer); 
        
        em.remove(pokemon);
        em.remove(trainer);
    }     
    
    public void testFindAllPokemon() {

        em.getTransaction().begin();
        Pokemon pokemon = new Pokemon();       
        pokemon.setName("name1");
        pokemon.setNick("nick1");        
        pokemon.setType(Type.FIGHTING);
        pokemon.setLevel(1);        
        Trainer trainer = new Trainer();
        trainer.setName("Pepa");
        trainer.setSurName("Zdepa");
        trainer.setBirthDate(new Date(1990, 4, 25));          
        pokemon.setTrainer(trainer); 
        
        Pokemon pokemon2 = new Pokemon();       
        pokemon2.setName("name2");
        pokemon2.setNick("nick2");        
        pokemon2.setType(Type.FIGHTING);
        pokemon2.setLevel(2);        
        Trainer trainer2 = new Trainer();
        trainer2.setName("Pepa");
        trainer2.setSurName("Zdepa");
        trainer2.setBirthDate(new Date(1990, 4, 25));          
        pokemon2.setTrainer(trainer2);         
        
        pdao.createPokemon(pokemon);
        pdao.createPokemon(pokemon2);
        tdao.createTrainer(trainer);
        tdao.createTrainer(trainer2);
        em.getTransaction().commit();

        List<Pokemon> accountList = pdao.findAllPokemon();
        assertTrue(accountList.size() == 2);
        assertTrue(accountList.contains(pokemon));
        assertTrue(accountList.contains(pokemon2));
        
        em.remove(pokemon);
        em.remove(pokemon2);
        em.remove(trainer);
        em.remove(trainer2);
    }    
    
    
  
} 

