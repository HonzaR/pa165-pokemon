/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.service;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.BadgeService;
import cz.muni.cz.backend.dao.BadgeDAO;
import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import cz.muni.cz.backend.service.impl.BadgeServiceImpl;
import cz.muni.cz.backend.service.impl.Convertor;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import property.Type;

/**
 * Badge service implementation tests
 * @author Tomas Sekera, 359902
 */
public class BadgeServiceImplTest {
    
    private BadgeService badgeService; 
    
    @Mock 
    private BadgeDAO badgeMock;
    
    
 
    @Before
    public void setUp() throws Exception {
        badgeMock = Mockito.mock(BadgeDAO.class);
        BadgeServiceImpl badgeServiceImpl = new BadgeServiceImpl();
        badgeServiceImpl.setBadgeDAO(badgeMock);
        badgeService = badgeServiceImpl;
    }

   
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testCreateBadge()
    {
        
//        try {
//            badgeService.createBadge(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        
        Mockito.doNothing().when(badgeMock).createBadge(Convertor.badgeDtoToEnt(badgeDTO));
        Mockito.verify(badgeMock, Mockito.never()).createBadge(null);
        
        badgeService.createBadge(badgeDTO);
        final Badge badge = Convertor.badgeDtoToEnt(badgeDTO);
        
        ArgumentCaptor<Badge> captor = ArgumentCaptor.forClass(Badge.class);
        Mockito.verify(badgeMock).createBadge(captor.capture());
        assertEquals(captor.getValue().getStadium(), badge.getStadium());
        
        Mockito.verify(badgeMock, Mockito.never()).updateBadge(Mockito.any(Badge.class));
        Mockito.verify(badgeMock, Mockito.never()).deleteBadge(Mockito.any(Badge.class));
   
    }
    
    @Test
    public void testUpdateBadge()
    {
//        try {
//            badgeService.updateBadge(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final StadiumDTO stadium2 = TestUtils.createTestStadiumObject(2L, "Praha", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        
        
        
        badgeService.createBadge(badgeDTO);
        Long id = badgeDTO.getId();
        
        badgeDTO.setStadium(stadium2);
        badgeService.updateBadge(badgeDTO);
        final Badge badge = Convertor.badgeDtoToEnt(badgeDTO);
        
        
        ArgumentCaptor<Badge> captor = ArgumentCaptor.forClass(Badge.class);
        Mockito.verify(badgeMock).createBadge(captor.capture());
        assertEquals(captor.getValue().getStadium(), badge.getStadium());
        
        Mockito.when(badgeMock.findBadgeById(id)).thenReturn(Convertor.badgeDtoToEnt(badgeDTO));
        BadgeDTO up = badgeService.findBadgeById(id);
        assertEquals(badgeDTO, up);

    }
    
    @Test
    public void updateDeleteBadge()
    {
//        try {
//            badgeService.deleteBadge(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        
        badgeService.deleteBadge(badgeDTO);
        Long id = badgeDTO.getId();
        
        Mockito.verify(badgeMock).deleteBadge(Mockito.any(Badge.class));        
        Mockito.when(badgeMock.findBadgeById(id)).thenReturn(null);
        
        // try to find deleted pokemon leads to exception
        Mockito.when(badgeMock.findBadgeById(id)).thenThrow(new DAOException("Badge doesn't exist.") {});
        try {
            BadgeDTO deleted = badgeService.findBadgeById(id);
            fail();
        }
        catch (DAOException e) {}          
                
        Mockito.verify(badgeMock, Mockito.never()).updateBadge(Mockito.any(Badge.class));
        Mockito.verify(badgeMock, Mockito.never()).createBadge(Mockito.any(Badge.class));
    }
    
    
    @Test
    public void testFindBadgeById()
    {
//        try {
//            badgeService.findBadgeById(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        
        
        badgeService.createBadge(badgeDTO);
        
        Mockito.when(badgeMock.findBadgeById(2l)).thenReturn(Convertor.badgeDtoToEnt(badgeDTO));
        
        assertEquals(badgeDTO, badgeService.findBadgeById(2l));
        Mockito.verify(badgeMock).findBadgeById(2l);
        Mockito.verify(badgeMock,Mockito.never()).deleteBadge(Convertor.badgeDtoToEnt(badgeDTO));
        Mockito.verify(badgeMock,Mockito.never()).updateBadge(Convertor.badgeDtoToEnt(badgeDTO)); 
        
        
    }
    
    @Test
    public void testFindAllBadges()
    {
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        final BadgeDTO badgeDTO2 = TestUtils.createTestBadgeObject(3L, stadium, trainer);
        
        List<BadgeDTO> badgeDTOList = new ArrayList<BadgeDTO>();
        Mockito.when(badgeMock.findAllBadges()).thenReturn(new ArrayList<Badge>());
        
        List<BadgeDTO> returnedBadgeDTOList = badgeService.findAllBadges();
        assertEquals(badgeDTOList, returnedBadgeDTOList);        
        assertEquals(returnedBadgeDTOList.size(), 0);
      
        badgeDTOList.add(badgeDTO);
        badgeDTOList.add(badgeDTO2);
        
        List<Badge> badgeEntList = new ArrayList<Badge>();
        for (BadgeDTO el : badgeDTOList) {
            badgeEntList.add(Convertor.badgeDtoToEnt(el));
        }
        
        Mockito.when(badgeMock.findAllBadges()).thenReturn(badgeEntList);
        assertEquals(badgeEntList.size(), 2);        
        
        returnedBadgeDTOList = badgeService.findAllBadges();
        assertEquals(badgeDTOList, returnedBadgeDTOList);        
        assertEquals(returnedBadgeDTOList.size(), 2);
    }
    
    @Test
    public void testFindBadgeByStadium()
    {
        
//        try {
//            badgeService.findBadgeByStadium(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        Mockito.verify(badgeMock, Mockito.never()).findBadgeByStadium(null);
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        final StadiumDTO emptyStadiumDTO = new StadiumDTO();
        final Stadium emptyStadium = Convertor.stadiumDtoToEnt(emptyStadiumDTO);
        final Stadium stadiumEnt = Convertor.stadiumDtoToEnt(stadium);

        Mockito.when(badgeMock.findBadgeByStadium(emptyStadium)).thenReturn(new ArrayList<Badge>());
        List<Badge> emptyList = new ArrayList<Badge>();
        assertEquals(emptyList, badgeService.findBadgeByStadium(emptyStadiumDTO));

        List<BadgeDTO> list = new ArrayList<BadgeDTO>();
        list.add(badgeDTO);
        
        List<Badge> entityList = new ArrayList<Badge>();
        entityList.add(Convertor.badgeDtoToEnt(badgeDTO));
        Mockito.when(badgeMock.findBadgeByStadium(stadiumEnt)).thenReturn(entityList);

        List<BadgeDTO> resultList = badgeService.findBadgeByStadium(stadium);
        ArgumentCaptor<Stadium> captor = ArgumentCaptor.forClass(Stadium.class);
        
        Mockito.verify(badgeMock, Mockito.atLeastOnce()).findBadgeByStadium(captor.capture());
        assertEquals(captor.getValue().getTrainer(), stadiumEnt.getTrainer());

        for (int i = 0; i < resultList.size(); i++) {
            assertEquals(list.get(i).getId(), resultList.get(i).getId());
            assertEquals(list.get(i).getStadium(), resultList.get(i).getStadium());
        }       
    }  
        
    
    
    @Test
    public void testFindBadgeByTrainer()
    {
//        try {
//            badgeService.findBadgeByTrainer(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        Mockito.verify(badgeMock, Mockito.never()).findBadgeByTrainer(null);
        
        final TrainerDTO trainer = TestUtils.createTestTrainerObject(2L, "Ash", "Kecup", new Date());
        final StadiumDTO stadium = TestUtils.createTestStadiumObject(2L, "Brno", trainer, Type.FIRE);
        final BadgeDTO badgeDTO = TestUtils.createTestBadgeObject(2L, stadium, trainer);
        final TrainerDTO emptyTrainerDTO = new TrainerDTO();
        final Trainer emptyTrainer = Convertor.trainerDtoToEnt(emptyTrainerDTO);
        final Trainer trainerEnt = Convertor.trainerDtoToEnt(trainer);

        Mockito.when(badgeMock.findBadgeByTrainer(emptyTrainer)).thenReturn(new ArrayList<Badge>());
        List<Badge> emptyList = new ArrayList<Badge>();
        assertEquals(emptyList, badgeService.findBadgeByTrainer(emptyTrainerDTO));

        List<BadgeDTO> list = new ArrayList<BadgeDTO>();
        list.add(badgeDTO);
        
        List<Badge> entityList = new ArrayList<Badge>();
        entityList.add(Convertor.badgeDtoToEnt(badgeDTO));
        Mockito.when(badgeMock.findBadgeByTrainer(trainerEnt)).thenReturn(entityList);

        List<BadgeDTO> resultList = badgeService.findBadgeByTrainer(trainer);
        ArgumentCaptor<Trainer> captor = ArgumentCaptor.forClass(Trainer.class);
        
        Mockito.verify(badgeMock, Mockito.atLeastOnce()).findBadgeByTrainer(captor.capture());
        assertEquals(captor.getValue().getName(), trainerEnt.getName());

        for (int i = 0; i < resultList.size(); i++) {
            assertEquals(list.get(i).getId(), resultList.get(i).getId());
            assertEquals(list.get(i).getTrainer(), resultList.get(i).getTrainer());
        }   
        
    }
    
}
