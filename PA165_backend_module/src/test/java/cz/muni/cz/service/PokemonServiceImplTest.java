/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.service;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.backend.dao.PokemonDAO;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Trainer;
import cz.muni.cz.backend.service.impl.Convertor;
import cz.muni.cz.backend.service.impl.PokemonServiceImpl;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static junit.framework.TestCase.fail;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import property.Type;

/**
 *
 * @author Jan Rychnovský
 */
public class PokemonServiceImplTest {
    
    @Mock
    private PokemonDAO pokemonDaoMock;
    private PokemonService pokemonService;
    private PokemonDTO pokemonDto;
    private PokemonDTO pokemonDto2;
    private TrainerDTO trainerDto;

    @Before
    public void setUp() throws Exception {
        
        pokemonDaoMock = mock(PokemonDAO.class);
        PokemonServiceImpl pokemonServiceImpl = new PokemonServiceImpl();
        pokemonServiceImpl.setPokemonDAO(pokemonDaoMock);
        pokemonService = pokemonServiceImpl;
    } 
    
    @Test
    public void testCreatePokemon() {
        
        
//        try {
//            pokemonService.createPokemon(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG);
        
        doNothing().when(pokemonDaoMock).createPokemon(Convertor.pokemonDtoToEnt(pokemonDto));
        verify(pokemonDaoMock, never()).createPokemon(null);
        
        pokemonService.createPokemon(pokemonDto);
        
        ArgumentCaptor<Pokemon> captor = ArgumentCaptor.forClass(Pokemon.class);
        verify(pokemonDaoMock).createPokemon(captor.capture());
        assertEquals(captor.getValue().getName(), pokemonDto.getName());
        
        verify(pokemonDaoMock, never()).updatePokemon(any(Pokemon.class));
        verify(pokemonDaoMock, never()).deletePokemon(any(Pokemon.class));
    } 
    
    @Test
    public void testUpdatePokemon() {
        
        
//        try {
//            pokemonService.createPokemon(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG);
        doNothing().when(pokemonDaoMock).updatePokemon(Convertor.pokemonDtoToEnt(pokemonDto));

        //create
        pokemonService.createPokemon(pokemonDto);
        Long id = pokemonDto.getId();
        
        ArgumentCaptor<Pokemon> captor = ArgumentCaptor.forClass(Pokemon.class);
        verify(pokemonDaoMock).createPokemon(captor.capture());
        assertEquals(captor.getValue().getName(), pokemonDto.getName());

        verify(pokemonDaoMock, never()).deletePokemon(any(Pokemon.class));
        
        //update
        pokemonDto.setName("Tomio");
        pokemonService.updatePokemon(pokemonDto);
        ArgumentCaptor<Pokemon> captor2 = ArgumentCaptor.forClass(Pokemon.class);
        verify(pokemonDaoMock).updatePokemon(captor2.capture());
        assertEquals(captor2.getValue().getName(), pokemonDto.getName());
        
        // mock dao
        when(pokemonDaoMock.findPokemonById(id)).thenReturn(Convertor.pokemonDtoToEnt(pokemonDto));
        PokemonDTO updated = pokemonService.findPokemonById(id);
        assertEquals(updated, pokemonDto);
    } 
    
    @Test
    public void testDeletePokemon() {
        
        
//        try {
//            pokemonService.deletePokemon(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG);
        
        pokemonService.deletePokemon(pokemonDto);
        Long id = pokemonDto.getId();
        
        verify(pokemonDaoMock).deletePokemon(any(Pokemon.class));        
        when(pokemonDaoMock.findPokemonById(id)).thenReturn(null);
        
        // try to find deleted pokemon leads to exception
        when(pokemonDaoMock.findPokemonById(id)).thenThrow(new DAOException("Pokemon doesn't exist.") {});
        try {
            PokemonDTO deleted = pokemonService.findPokemonById(id);
            fail();
        }
        catch (DAOException e) {}          
                
        verify(pokemonDaoMock, never()).updatePokemon(any(Pokemon.class));
        verify(pokemonDaoMock, never()).createPokemon(any(Pokemon.class));
    }  
    
    @Test
    public void testFindPokemonById() {
        
        
        doThrow(new DAOException("Object not found") {}).when(pokemonDaoMock).findPokemonById(-1L);
        
//        try {
//            pokemonService.findPokemonById(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        try {
            pokemonService.findPokemonById(-1L);
            fail();
        } catch (DAOException e) {
        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG); 
        
        pokemonService.createPokemon(pokemonDto);
        when(pokemonDaoMock.findPokemonById(1l)).thenReturn(Convertor.pokemonDtoToEnt(pokemonDto));
        
        assertEquals(pokemonDto, pokemonService.findPokemonById(1l));
        verify(pokemonDaoMock).findPokemonById(1l);
        verify(pokemonDaoMock,never()).deletePokemon(Convertor.pokemonDtoToEnt(pokemonDto));
        verify(pokemonDaoMock,never()).updatePokemon(Convertor.pokemonDtoToEnt(pokemonDto));       
    }
    
    @Test
    public void testFindAllPokemon() {
        
           
        List<PokemonDTO> pokemonDtoList = new ArrayList<PokemonDTO>();
        when(pokemonDaoMock.findAllPokemon()).thenReturn(new ArrayList<Pokemon>());
        
        List<PokemonDTO> returnedPokemonDtoList = pokemonService.findAllPokemon();
        assertEquals(pokemonDtoList, returnedPokemonDtoList);        
        assertEquals(returnedPokemonDtoList.size(), 0);
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        PokemonDTO firstDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG); 
        PokemonDTO secondDto = TestUtils.createTestPokemonObject(2L, "Name", "Pepe", 2, trainerDto, Type.DARK); 
        PokemonDTO thirdDto = TestUtils.createTestPokemonObject(3L, "Meno", "Cepe", 3, trainerDto, Type.DRAGON); 
        pokemonDtoList.add(firstDto);
        pokemonDtoList.add(secondDto);
        pokemonDtoList.add(thirdDto);
        
        List<Pokemon> pokemonEntList = new ArrayList<Pokemon>();
        for (PokemonDTO element : pokemonDtoList) {
            pokemonEntList.add(Convertor.pokemonDtoToEnt(element));
        }
        when(pokemonDaoMock.findAllPokemon()).thenReturn(pokemonEntList);
        assertEquals(pokemonEntList.size(), 3);        
        
        returnedPokemonDtoList = pokemonService.findAllPokemon();
        assertEquals(pokemonDtoList, returnedPokemonDtoList);        
        assertEquals(returnedPokemonDtoList.size(), 3);        


    }   
    
    @Test
    public void testFindPokemonByName() {
        
        
//        try {
//            pokemonService.findPokemonByName(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG); 
        
        when(pokemonDaoMock.findPokemonByName(pokemonDto.getName())).thenReturn(null);

        List<Pokemon> pokemons = new ArrayList<Pokemon>();
        pokemons.add(Convertor.pokemonDtoToEnt(pokemonDto));

        when(pokemonDaoMock.findPokemonByName("Tomas")).thenReturn(pokemons);
        when(pokemonDaoMock.findPokemonByName("Test")).thenReturn(new ArrayList<Pokemon>());

        assertTrue(pokemonService.findPokemonByName("Tomas").contains(pokemonDto));
        assertEquals(pokemonService.findPokemonByName("Tomas").size(), 1);
        assertEquals(pokemonService.findPokemonByName("Test").size(), 0);       
    }  
    
    @Test
    public void testFindPokemonByNick() {
        
        
//        try {
//            pokemonService.findPokemonByNick(null);
//            fail();
//        } catch (DAOException ex) {
//        }        
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG);  
        pokemonDto2 = TestUtils.createTestPokemonObject(2L, "Tomio", "Pepio", 2, trainerDto, Type.DARK);
        PokemonDTO returnedDto;
        
        // test - no entity is in table
        when(pokemonDaoMock.findPokemonByNick(pokemonDto.getNick())).thenThrow(new DAOException("Account is null.") {});
        try {
            returnedDto = pokemonService.findPokemonByNick(pokemonDto.getNick());
            fail();
        }
        catch (DAOException e) {}        
        
        // test - some entity in table 
        when(pokemonDaoMock.findPokemonByNick(pokemonDto2.getNick())).thenReturn(Convertor.pokemonDtoToEnt(pokemonDto2));
        returnedDto = pokemonService.findPokemonByNick(pokemonDto2.getNick());
        assertEquals(pokemonDto2.getId(), returnedDto.getId());
        assertEquals(pokemonDto2.getName(), returnedDto.getName());
        assertEquals(pokemonDto2.getNick(), returnedDto.getNick());
        assertEquals(pokemonDto2.getLevel(), returnedDto.getLevel());       
        assertEquals(pokemonDto2.getTrainer(), returnedDto.getTrainer());        
        assertEquals(pokemonDto2.getType(), returnedDto.getType());        

        verify(pokemonDaoMock).findPokemonByNick(pokemonDto.getNick());  
        verify(pokemonDaoMock).findPokemonByNick(pokemonDto2.getNick());
    } 
    
    @Test
    public void testFindPokemonByLevel() {
        
        
//        try {
//            pokemonService.findPokemonByLevel(-1);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG); 
        
        when(pokemonDaoMock.findPokemonByLevel(pokemonDto.getLevel())).thenReturn(null);

        List<Pokemon> pokemons = new ArrayList<Pokemon>();
        pokemons.add(Convertor.pokemonDtoToEnt(pokemonDto));

        when(pokemonDaoMock.findPokemonByLevel(1)).thenReturn(pokemons);
        when(pokemonDaoMock.findPokemonByLevel(2)).thenReturn(new ArrayList<Pokemon>());

        assertTrue(pokemonService.findPokemonByLevel(1).contains(pokemonDto));
        assertEquals(pokemonService.findPokemonByLevel(1).size(), 1);
        assertEquals(pokemonService.findPokemonByLevel(2).size(), 0);       
    }     
    
    @Test
    public void testFindPokemonByType() {
        
          
//        try {
//            pokemonService.findPokemonByType(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG); 
        
        when(pokemonDaoMock.findPokemonByType(pokemonDto.getType())).thenReturn(null);

        List<Pokemon> pokemons = new ArrayList<Pokemon>();
        pokemons.add(Convertor.pokemonDtoToEnt(pokemonDto));

        when(pokemonDaoMock.findPokemonByType(Type.BUG)).thenReturn(pokemons);
        when(pokemonDaoMock.findPokemonByType(Type.FAIRY)).thenReturn(new ArrayList<Pokemon>());

        assertTrue(pokemonService.findPokemonByType(Type.BUG).contains(pokemonDto));
        assertEquals(pokemonService.findPokemonByType(Type.BUG).size(), 1);
        assertEquals(pokemonService.findPokemonByType(Type.FAIRY).size(), 0);       
    } 
    
    @Test
    public void testFindPokemonByTrainer() {
        
           
//        try {
//            pokemonService.findPokemonByTrainer(null);
//            fail();
//        } catch (DAOException e) {
//
//        }
        verify(pokemonDaoMock, never()).findPokemonByTrainer(null);

        TrainerDTO nonExistingTrainerDto = new TrainerDTO();
        Trainer nonExistingTrainer = Convertor.trainerDtoToEnt(nonExistingTrainerDto);

        when(pokemonDaoMock.findPokemonByTrainer(nonExistingTrainer)).thenReturn(new ArrayList<Pokemon>());
        List<Pokemon> emptyList = new ArrayList<Pokemon>();
        assertEquals(emptyList, pokemonService.findPokemonByTrainer(nonExistingTrainerDto));

        ArgumentCaptor<Trainer> captor1 = ArgumentCaptor.forClass(Trainer.class);
        verify(pokemonDaoMock, times(1)).findPokemonByTrainer(captor1.capture());
        assertEquals(captor1.getValue().getName(), nonExistingTrainer.getName());
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        pokemonDto = TestUtils.createTestPokemonObject(1L, "Tomas", "Seky", 1, trainerDto, Type.BUG);        
        Trainer existingTrainer = Convertor.trainerDtoToEnt(trainerDto);
        
        List<PokemonDTO> existingList = new ArrayList<PokemonDTO>();
        existingList.add(pokemonDto);
        
        List<Pokemon> entityList = new ArrayList<Pokemon>();
        entityList.add(Convertor.pokemonDtoToEnt(pokemonDto));
        when(pokemonDaoMock.findPokemonByTrainer(existingTrainer)).thenReturn(entityList);

        List<PokemonDTO> resultList = pokemonService.findPokemonByTrainer(trainerDto);
        ArgumentCaptor<Trainer> captor2 = ArgumentCaptor.forClass(Trainer.class);
        verify(pokemonDaoMock, atLeastOnce()).findPokemonByTrainer(captor2.capture());
        assertEquals(captor2.getValue().getName(), existingTrainer.getName());

        for (int i = 0; i < resultList.size(); i++) {
            assertEquals(existingList.get(i).getId(), resultList.get(i).getId());
            assertEquals(existingList.get(i).getName(), resultList.get(i).getName());
        }       
    }     
    
    
    
}
