/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.service;

import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.StadiumService;
import cz.muni.cz.backend.dao.StadiumDAO;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.service.impl.Convertor;
import cz.muni.cz.backend.service.impl.StadiumServiceImpl;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static junit.framework.TestCase.fail;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import property.Type;

/**
 *
 * @author Tommy
 */
public class StadiumServiceImplTest {
    
    @Mock
    private StadiumDAO stadiumDaoMock;
    private StadiumService stadiumService;
    private StadiumDTO stadiumDto;
    private StadiumDTO stadiumDto2;
    private TrainerDTO trainerDto;

    @Before
    public void setUp() throws Exception {
        
        stadiumDaoMock = mock(StadiumDAO.class);
        StadiumServiceImpl stadiumServiceImpl = new StadiumServiceImpl();
        stadiumServiceImpl.setStadiumDAO(stadiumDaoMock);
        stadiumService = stadiumServiceImpl;
    } 
    
    @Test
    public void testCreateStadium() {
        
        
//        try {
//            stadiumService.createStadium(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        
        doNothing().when(stadiumDaoMock).createStadium(Convertor.stadiumDtoToEnt(stadiumDto));
        verify(stadiumDaoMock, never()).createStadium(null);
        
        stadiumService.createStadium(stadiumDto);
        
        ArgumentCaptor<Stadium> captor = ArgumentCaptor.forClass(Stadium.class);
        verify(stadiumDaoMock).createStadium(captor.capture());
        assertEquals(captor.getValue().getCity(), stadiumDto.getCity());
        
        verify(stadiumDaoMock, never()).updateStadium(any(Stadium.class));
        verify(stadiumDaoMock, never()).deleteStadium(any(Stadium.class));
    } 
    
    @Test
    public void testUpdateStadium() {
        
        
//        try {
//            stadiumService.createStadium(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        doNothing().when(stadiumDaoMock).updateStadium(Convertor.stadiumDtoToEnt(stadiumDto));

        //create
        stadiumService.createStadium(stadiumDto);
        Long id = stadiumDto.getId();
        
        ArgumentCaptor<Stadium> captor = ArgumentCaptor.forClass(Stadium.class);
        verify(stadiumDaoMock).createStadium(captor.capture());
        assertEquals(captor.getValue().getCity(), stadiumDto.getCity());

        verify(stadiumDaoMock, never()).deleteStadium(any(Stadium.class));
        
        //update
        stadiumDto.setCity("Pewter City");
        stadiumService.updateStadium(stadiumDto);
        ArgumentCaptor<Stadium> captor2 = ArgumentCaptor.forClass(Stadium.class);
        verify(stadiumDaoMock).updateStadium(captor2.capture());
        assertEquals(captor2.getValue().getCity(), stadiumDto.getCity());
        
        // mock dao
        when(stadiumDaoMock.findStadiumById(id)).thenReturn(Convertor.stadiumDtoToEnt(stadiumDto));
        StadiumDTO updated = stadiumService.findStadiumById(id);
        assertEquals(updated, stadiumDto);
    } 
    
    @Test
    public void testDeleteStadium() {
        
          
//        try {
//            stadiumService.deleteStadium(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        
        stadiumService.deleteStadium(stadiumDto);
        Long id = stadiumDto.getId();
        
        verify(stadiumDaoMock).deleteStadium(any(Stadium.class));        
        when(stadiumDaoMock.findStadiumById(id)).thenReturn(null);
        
        // try to find deleted pokemon leads to exception
        when(stadiumDaoMock.findStadiumById(id)).thenThrow(new DAOException("Stadium doesn't exist.") {});
        try {
            StadiumDTO deleted = stadiumService.findStadiumById(id);
            fail();
        }
        catch (DAOException e) {}          
                
        verify(stadiumDaoMock, never()).updateStadium(any(Stadium.class));
        verify(stadiumDaoMock, never()).createStadium(any(Stadium.class));
    }  
    
    @Test
    public void testFindStadiumById() {
        
        
        doThrow(new DAOException("Object not found") {}).when(stadiumDaoMock).findStadiumById(-1L);
        
//        try {
//            stadiumService.findStadiumById(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        try {
            stadiumService.findStadiumById(-1L);
            fail();
        } catch (DAOException e) {
        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        
        stadiumService.createStadium(stadiumDto);
        when(stadiumDaoMock.findStadiumById(1l)).thenReturn(Convertor.stadiumDtoToEnt(stadiumDto));
        
        assertEquals(stadiumDto, stadiumService.findStadiumById(1l));
        verify(stadiumDaoMock).findStadiumById(1l);
        verify(stadiumDaoMock,never()).deleteStadium(Convertor.stadiumDtoToEnt(stadiumDto));
        verify(stadiumDaoMock,never()).updateStadium(Convertor.stadiumDtoToEnt(stadiumDto));       
    }
    
    @Test
    public void testFindAllStadiums() {
        
         
        List<StadiumDTO> stadiumDtoList = new ArrayList<StadiumDTO>();
        when(stadiumDaoMock.findAllStadiums()).thenReturn(new ArrayList<Stadium>());
        
        List<StadiumDTO> returnedStadiumDtoList = stadiumService.findAllStadiums();
        assertEquals(stadiumDtoList, returnedStadiumDtoList);        
        assertEquals(returnedStadiumDtoList.size(), 0);
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        stadiumDto2 = TestUtils.createTestStadiumObject(1L, "Twinleaf Town", trainerDto, Type.GRASS);
        StadiumDTO stadiumDto3 = TestUtils.createTestStadiumObject(1L, "Littleroot Town", trainerDto, Type.ELECTRIC); 
        stadiumDtoList.add(stadiumDto);
        stadiumDtoList.add(stadiumDto2);
        stadiumDtoList.add(stadiumDto3);
        
        List<Stadium> stadiumEntList = new ArrayList<Stadium>();
        for (StadiumDTO element : stadiumDtoList) {
            stadiumEntList.add(Convertor.stadiumDtoToEnt(element));
        }
        when(stadiumDaoMock.findAllStadiums()).thenReturn(stadiumEntList);
        assertEquals(stadiumEntList.size(), 3);        
        
        returnedStadiumDtoList = stadiumService.findAllStadiums();
        assertEquals(stadiumDtoList, returnedStadiumDtoList);        
        assertEquals(returnedStadiumDtoList.size(), 3);        


    }   
    
    @Test
    public void testFindStadiumByCity() {
        
         
//        try {
//            stadiumService.findStadiumByCity(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        
        when(stadiumDaoMock.findStadiumByCity(stadiumDto.getCity())).thenReturn(null);

        List<Stadium> stadiums = new ArrayList<Stadium>();
        stadiums.add(Convertor.stadiumDtoToEnt(stadiumDto));

        when(stadiumDaoMock.findStadiumByCity("Pallet Town")).thenReturn(stadiums);
        when(stadiumDaoMock.findStadiumByCity("Test")).thenReturn(new ArrayList<Stadium>());

        assertTrue(stadiumService.findStadiumByCity("Pallet Town").contains(stadiumDto));
        assertEquals(stadiumService.findStadiumByCity("Pallet Town").size(), 1);
        assertEquals(stadiumService.findStadiumByCity("Test").size(), 0);       
    }  
    
    @Test
    public void testFindStadiumByType() {
        
        
//        try {
//            stadiumService.findStadiumByType(null);
//            fail();
//        } catch (DAOException ex) {
//        }        
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);

        when(stadiumDaoMock.findStadiumByType(stadiumDto.getType())).thenReturn(null);

        List<Stadium> stadiums = new ArrayList<Stadium>();
        stadiums.add(Convertor.stadiumDtoToEnt(stadiumDto));

        when(stadiumDaoMock.findStadiumByType(Type.FIRE)).thenReturn(stadiums);
        when(stadiumDaoMock.findStadiumByType(Type.FAIRY)).thenReturn(new ArrayList<Stadium>());

        assertTrue(stadiumService.findStadiumByType(Type.FIRE).contains(stadiumDto));
        assertEquals(stadiumService.findStadiumByType(Type.FIRE).size(), 1);
        assertEquals(stadiumService.findStadiumByType(Type.FAIRY).size(), 0);       
    } 
    
    @Test
    public void testFindStadiumByTrainer() {
        
        
//        try {
//            stadiumService.findStadiumByTrainer(null);
//            fail();
//        } catch (DAOException e) {
//
//        }
        verify(stadiumDaoMock, never()).findStadiumByTrainer(null);

        TrainerDTO nonExistingTrainerDto = new TrainerDTO();
        
//        try {
//            stadiumService.findStadiumByTrainer(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        trainerDto = TestUtils.createTestTrainerObject(1L, "Martin", "Prokop", new Date(1990, 4, 25));
        stadiumDto = TestUtils.createTestStadiumObject(1L, "Pallet Town", trainerDto, Type.FIRE);
        
        stadiumService.createStadium(stadiumDto);
        when(stadiumDaoMock.findStadiumByTrainer(Convertor.trainerDtoToEnt(trainerDto))).thenReturn(Convertor.stadiumDtoToEnt(stadiumDto));
        
        assertEquals(stadiumDto, stadiumService.findStadiumByTrainer(trainerDto));
        verify(stadiumDaoMock).findStadiumByTrainer(Convertor.trainerDtoToEnt(trainerDto));
        verify(stadiumDaoMock,never()).deleteStadium(Convertor.stadiumDtoToEnt(stadiumDto));
        verify(stadiumDaoMock,never()).updateStadium(Convertor.stadiumDtoToEnt(stadiumDto));       
    }
    
}
