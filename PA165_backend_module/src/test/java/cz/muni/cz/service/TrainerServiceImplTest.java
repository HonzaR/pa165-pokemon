/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.service;

import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.TrainerService;
import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.entities.Trainer;
import cz.muni.cz.backend.service.impl.Convertor;
import cz.muni.cz.backend.service.impl.TrainerServiceImpl;
import cz.muni.cz.backend.dao.impl.DAOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Martin Prokop
 */
public class TrainerServiceImplTest {
    
    @Mock
    private TrainerDAO trainerDaoMock;
    
    private TrainerService trainerService;
    private TrainerDTO trainerDto;
    private TrainerDTO trainerDto2;
    private TrainerDTO trainerDto3;
    
    @Before
    public void setUp() throws Exception {
        
        trainerDaoMock = mock(TrainerDAO.class);
        TrainerServiceImpl trainerServiceImpl = new TrainerServiceImpl();
        trainerServiceImpl.setTrainerDAO(trainerDaoMock);
        trainerService = trainerServiceImpl;
        
    }
    
    @Test
    public void testCreateTrainer() {
        
//        try {
//            trainerService.createTrainer(null);
//            fail();
//        } catch (DAOException ex) {
//        }
        
        verify(trainerDaoMock, never()).createTrainer(null);
        //doNothing().when(trainerDaoMock).createTrainer(Convertor.trainerDtoToEnt(trainerDto));
        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));
        ArgumentCaptor<Trainer> captor = ArgumentCaptor.forClass(Trainer.class);
        trainerService.createTrainer(trainerDto);
        Trainer trainer = Convertor.trainerDtoToEnt(trainerDto);
        
        verify(trainerDaoMock).createTrainer(captor.capture());
        assertEquals(captor.getValue().getName(), trainer.getName());
        assertEquals(captor.getValue().getSurName(), trainer.getSurName());
        assertEquals(captor.getValue().getBirthDate(), trainer.getBirthDate());
        verify(trainerDaoMock, never()).updateTrainer(any(Trainer.class));
        verify(trainerDaoMock, never()).deleteTrainer(any(Trainer.class));
    }

    @Test
    public void testUpdateTrainer() {

//        try {
//            trainerService.updateTrainer(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));
        Long id = trainerDto.getId();
        trainerService.createTrainer(trainerDto);

        trainerDto.setName("Pepa");
        trainerDto.setSurName("Zdepa");
        trainerDto.setBirthDate(new Date(1987, 4, 12));

        trainerService.updateTrainer(trainerDto);
        ArgumentCaptor<Trainer> captor = ArgumentCaptor.forClass(Trainer.class);
        Trainer trainer = Convertor.trainerDtoToEnt(trainerDto);
        
        verify(trainerDaoMock).updateTrainer(captor.capture());
        assertEquals(captor.getValue().getName(), trainer.getName());
        assertEquals(captor.getValue().getSurName(), trainer.getSurName());
        assertEquals(captor.getValue().getBirthDate(), trainer.getBirthDate());

        when(trainerDaoMock.findTrainerById(id)).thenReturn(Convertor.trainerDtoToEnt(trainerDto));
        
        TrainerDTO trainerDto2 = trainerService.findTrainerById(id);
        assertEquals(trainerDto, trainerDto2);
        
        Trainer trainer2 = Convertor.trainerDtoToEnt(trainerDto2);
        assertEquals(trainerDto.getName(), trainer2.getName());
        assertEquals(trainerDto.getSurName(), trainer2.getSurName());
        assertEquals(trainerDto.getBirthDate(), trainer2.getBirthDate());
    }
    
    @Test
    public void testDeleteTrainer() {

//        try {
//            trainerService.deleteTrainer(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));
        trainerService.deleteTrainer(trainerDto);
        Long id = trainerDto.getId();
        verify(trainerDaoMock).deleteTrainer(Convertor.trainerDtoToEnt(trainerDto));

        when(trainerDaoMock.findTrainerById(id)).thenThrow(new DAOException("Trainer does not exist.") {});
        try {
            TrainerDTO deleted = trainerService.findTrainerById(id);
            fail();
        }
        catch (DAOException e) {}
    }
    
    @Test
    public void testFindAllTrainer() {

        List<TrainerDTO> trainerDtoList = new ArrayList<TrainerDTO>();
        when(trainerDaoMock.findAllTrainer()).thenReturn(new ArrayList<Trainer>());

        List<TrainerDTO> trainerDtoList2 = trainerService.findAllTrainer();
        assertTrue(trainerDtoList2.isEmpty());

        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));
        trainerDto2 = TestUtils.createTestTrainerObject(1L, "Pepa", "Zdepa", new Date(1987, 4, 12));
        trainerDtoList.add(trainerDto);
        trainerDtoList.add(trainerDto2);

        List<Trainer> entities = new ArrayList<Trainer>();
        for (TrainerDTO trainerLoc : trainerDtoList) {
            entities.add(Convertor.trainerDtoToEnt(trainerLoc));
        }
        when(trainerDaoMock.findAllTrainer()).thenReturn(entities);
        assertTrue(entities.size() == 2);
        
        List<TrainerDTO> trainerDtoList3 = trainerService.findAllTrainer();
        assertTrue(trainerDtoList3.size() == 2);
        for (TrainerDTO trainerLoc : trainerDtoList) {
            assertTrue(trainerDtoList3.contains(trainerLoc));
        }
    }
    
    @Test
    public void testFindTrainerById() {

//        try {
//            trainerService.findTrainerById(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));

        when(trainerDaoMock.findTrainerById(trainerDto.getId())).thenThrow(new DAOException("Trainer is null") {});
        try {
            trainerDto2 = trainerService.findTrainerById(trainerDto.getId());
            fail();
        }
        catch (DAOException e) {}

        trainerDto3 = TestUtils.createTestTrainerObject(2L, "Pepa", "Zdepa", new Date(1987, 4, 12));
        when(trainerDaoMock.findTrainerById(trainerDto3.getId())).thenReturn(Convertor.trainerDtoToEnt(trainerDto3));
        trainerDto2 = trainerService.findTrainerById(trainerDto3.getId());
        assertEquals(trainerDto2.getId(), trainerDto3.getId());
        assertEquals(trainerDto2.getName(), trainerDto3.getName());
        assertEquals(trainerDto2.getSurName(), trainerDto3.getSurName());
        assertEquals(trainerDto2.getBirthDate(), trainerDto3.getBirthDate());
    }
    
    @Test
    public void testFindTrainerByName() {

//        try {
//            trainerService.findTrainerByName(null);
//            fail();
//        } catch (DAOException ex) {
//        }

        trainerDto = TestUtils.createTestTrainerObject(1L, "Hanzik", "Panzik", new Date(1989, 3, 23));

        when(trainerDaoMock.findTrainerByName(trainerDto.getName())).thenThrow(new DAOException("Trainer is null") {});
        try {
            trainerDto2 = trainerService.findTrainerByName(trainerDto.getName());
            fail();
        }
        catch (DAOException e) {}

        trainerDto3 = TestUtils.createTestTrainerObject(1L, "Pepa", "Zdepa", new Date(1987, 4, 12));
        when(trainerDaoMock.findTrainerByName(trainerDto3.getName())).thenReturn(Convertor.trainerDtoToEnt(trainerDto3));
        trainerDto2 = trainerService.findTrainerByName(trainerDto3.getName());
        assertEquals(trainerDto2.getId(), trainerDto3.getId());
        assertEquals(trainerDto2.getName(), trainerDto3.getName());
        assertEquals(trainerDto2.getSurName(), trainerDto3.getSurName());
        assertEquals(trainerDto2.getBirthDate(), trainerDto3.getBirthDate());
    }
    
}
