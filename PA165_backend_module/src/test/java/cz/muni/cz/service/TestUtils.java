/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.service;

import cz.muni.cz.api.dto.*;
import java.util.Date;
import property.Type;

/**
 *
 * @author Jan Rychnovský
 */
public class TestUtils {
    
    public static PokemonDTO createTestPokemonObject(Long id, String name, String nick, int rank, TrainerDTO trainer, Type type) {
        
        PokemonDTO pokemon = new PokemonDTO();
        pokemon.setId(id);
        pokemon.setName(name);
        pokemon.setNick(nick);
        pokemon.setLevel(rank);
        pokemon.setTrainer(trainer);

        return pokemon;
    } 
    
    public static TrainerDTO createTestTrainerObject(Long id, String name, String surname, Date birthDate) {
        
        TrainerDTO trainer = new TrainerDTO();
        trainer.setId(id);
        trainer.setName(name);
        trainer.setSurName(surname);
        trainer.setBirthDate(birthDate);

        return trainer;
    }    
    
    public static BadgeDTO createTestBadgeObject(Long id, StadiumDTO stadium, TrainerDTO trainer) {
        
        BadgeDTO Badge = new BadgeDTO();
        Badge.setId(id);
        Badge.setStadium(stadium);
        Badge.setTrainer(trainer);
        return Badge;
    } 
    
    public static StadiumDTO createTestStadiumObject(Long id, String name, TrainerDTO trainer, Type type) {
        
        StadiumDTO stadium = new StadiumDTO();
        stadium.setId(id);
        stadium.setCity(name);
        stadium.setTrainer(trainer); 
        stadium.setType(type);

        return stadium;
    } 
    
}
