package cz.muni.cz.backend.dao.impl;

import org.springframework.dao.DataAccessException;

/**
 *
 * @author Martin Prokop
 */
public class DAOException extends DataAccessException {

    public DAOException(String msg) {
        super(msg);
    }
    
    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
