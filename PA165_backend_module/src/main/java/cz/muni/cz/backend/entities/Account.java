package cz.muni.cz.backend.entities;

import javax.persistence.*;
import java.io.Serializable;

/*
 * Account entity
 *
 * @author: Tomas Sladek, 359532
 */
@Entity
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountSequence")
    @SequenceGenerator(name = "AccountSequence", sequenceName = "ACCOUNT_SEQ", initialValue = 100)
    private Long id;
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private Boolean isAdmin;
    @Column(nullable = false)
    private Long trainerID;

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTrainerID() {
        return trainerID;
    }

    public void setTrainerID(Long id) {
        this.trainerID = id;
    }
}
