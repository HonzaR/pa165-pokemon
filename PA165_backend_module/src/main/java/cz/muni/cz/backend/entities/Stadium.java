/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.backend.entities;

import property.Type;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


/**
 *
 * @author Jan Rychnovsk�, 430642
 */

@Entity
public class Stadium implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;  
    
    @Column(nullable=false)
    private String city;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable=false)
    private Type type;
    
    @OneToOne(optional = true, cascade = CascadeType.MERGE)
    private Trainer trainer;

    /**
     *
     */
    public Stadium() {
    }

    /**
     *
     * @param id
     * @param city
     * @param type
     * @param trainer
     */
    public Stadium(Long id, String city, Type type, Trainer trainer) {
        this.id = id;
        this.city = city;
        this.type = type;
        this.trainer = trainer;
    }
        
    /**
     *
     * @return id of stadium
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id set id of stadium
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     *
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     */
    public Type getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public Trainer getTrainer() {
        return trainer;
    }

    /**
     *
     * @param trainer
     */
    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 73 * hash + (this.city != null ? this.city.hashCode() : 0);
        hash = 73 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 73 * hash + (this.trainer != null ? this.trainer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stadium other = (Stadium) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Stadium{" + "id=" + id + ", city=" + city + ", type=" + type + ", trainer=" + trainer + '}';
    }
        
}
