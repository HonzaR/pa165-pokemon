package cz.muni.cz.backend.dao;

import cz.muni.cz.backend.entities.Account;
import javax.persistence.EntityManager;
import java.util.List;

/*
 * Account DAO object interface
 *
 * @author: Tomas Sladek, 359532
 */
public interface AccountDAO {

    public List<Account> findAll();

    public Account findAccountByName(String name);

    public Account findAccountById(Long id);

    public void createAccount(Account acc);

    public void updateAccount(Account acc);

    public void deleteAccount(Account acc);

    public void setManagerContext(EntityManager entityManager);
}
