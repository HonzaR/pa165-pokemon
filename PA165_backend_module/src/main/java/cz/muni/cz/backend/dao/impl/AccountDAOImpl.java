package cz.muni.cz.backend.dao.impl;

import cz.muni.cz.backend.dao.AccountDAO;
import cz.muni.cz.backend.entities.Account;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;


/*
 * Account DAO implementation
 *
 * @author: Tomas Sladek, 359532
 */
@Repository
public class AccountDAOImpl implements AccountDAO {

    @PersistenceContext
    private EntityManager manager;

    public void setManagerContext(EntityManager manager) {
        this.manager = manager;
    }

    public void createAccount(Account acc)
            throws DAOException, IllegalArgumentException {
        if (acc == null) {
            throw new IllegalArgumentException("Account is null.");
        }
        if (acc.getId() != null) {
            throw new IllegalArgumentException("Account ID is already set.");
        }
        if (acc.getPassword() == null) {
            throw new IllegalArgumentException("Account password is null");
        }
        if (acc.getName() == null) {
            throw new IllegalArgumentException("Account name is null");
        }
        try {
            manager.persist(acc);
        } catch (Exception ex) {
            throw new DAOException("Account " + acc + " not created", ex);
        }
    }

    public void updateAccount(Account acc)
            throws DAOException, IllegalArgumentException {
        if (acc == null) {
            throw new IllegalArgumentException("Account is null.");
        }
        if (acc.getId() == null) {
            throw new IllegalArgumentException("Account ID is null");
        }
        if (acc.getName() == null) {
            throw new IllegalArgumentException("Account name is null");
        }
        try {
            Account find = manager.find(Account.class, acc.getId());
            if (find == null) {
                throw new DAOException("Account " + acc + " does not exist.");
            }
            manager.merge(acc);
        } catch (Exception ex) {
            throw new DAOException("Account " + acc + " not updated", ex);
        }
    }

    public void deleteAccount(Account acc)
            throws DAOException, IllegalArgumentException {
        if (acc == null) {
            throw new IllegalArgumentException("Account is null.");
        }
        if (acc.getId() == null) {
            throw new IllegalArgumentException("Account ID is null");
        }

        try {
            Account find = manager.find(Account.class, acc.getId());
            if (find == null) {
                throw new DAOException("Account " + acc + " does not exist.");
            }
            manager.remove(manager.contains(acc) ? acc : manager.merge(acc));
        } catch (Exception ex) {
            throw new DAOException("Account " + acc + " not deleted", ex);
        }
    }

    public Account findAccountById(Long id)
            throws DAOException, IllegalArgumentException {
        if (id == null) {
            throw new IllegalArgumentException("Cannot find account, id is null");
        }

        try {
            Account find = manager.find(Account.class, id);
            if (find == null) {
                throw new DAOException("Account with id " + id + " does not exist.");
            }
            return find;
        } catch (Exception ex) {
            throw new DAOException("Account with id " + id + " not found", ex);
        }
    }

    public List<Account> findAll()
            throws DAOException {
        List<Account> accs;
        try {
            accs = manager.createQuery("SELECT a FROM Account a").getResultList();
            return accs;
        } catch (Exception ex) {
            throw new DAOException("All accounts not found", ex);
        }
    }

    public Account findAccountByName(String name) {

        if (name == null) {
            throw new IllegalArgumentException("Name is null");
        }

        List<Account> acc = manager.createQuery("SELECT a FROM Account a WHERE a.name= :name").setParameter("name", name).getResultList();
        if (acc.isEmpty()) {
            return null;
        } else {
            return acc.get(0);
        }
    }

}
