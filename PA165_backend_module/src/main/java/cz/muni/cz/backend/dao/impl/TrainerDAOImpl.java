package cz.muni.cz.backend.dao.impl;

import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jan Rychnovský, 430642
 */
@Repository
public class TrainerDAOImpl implements TrainerDAO {
    
    @PersistenceContext
    private EntityManager em;

    
    public void setManagerContext(EntityManager manager)
    {
        this.em = manager;
    }     

    /**
     * Return entity manager factory
     * @return emf Entity Manager Factory
     */
    public EntityManager getEm() {
        return em;
    }

    /**
     * Set new entity manager factory
     * @param emf Entity Manager Factory
     */
    private void setEm(EntityManager em) {
        this.em = em;
    }

    /**
     * create new Trainer
     * @param trainer Trainer entity
     */
    public void createTrainer(Trainer trainer) throws IllegalArgumentException, DAOException {
        
        if(trainer == null){
            throw new IllegalArgumentException("Trainer is null.");
        }
        if (trainer.getId() != null) {
            throw new IllegalArgumentException("Trainer " + trainer.getName() + " " + trainer.getSurName() + " already exists");
        }
        if(trainer.getName() == null){
            throw new IllegalArgumentException("Trainer's name is null.");
        }
        if(trainer.getSurName() == null){
            throw new IllegalArgumentException("Trainer's surname is null.");
        }
        if(trainer.getBirthDate() == null){
            throw new IllegalArgumentException("Trainer's birthday is null.");
        }        
        
        try {
           em.persist(trainer);
        } catch (Exception e) {
            throw new DAOException("Error creating trainer.", e);
        }
    }

    /**
     * update Trainer
     * @param trainer Trainer entity
     */
    public void updateTrainer(Trainer trainer) throws IllegalArgumentException, DAOException {
        
        if(trainer == null){
            throw new IllegalArgumentException("Trainer is null.");
        }
        if (trainer.getId() == null) {
            throw new IllegalArgumentException("Trainer " + trainer.getName() + " " + trainer.getSurName() + " already exists");
        }
        if(trainer.getName() == null){
            throw new IllegalArgumentException("Trainer's name is null.");
        }
        if(trainer.getSurName() == null){
            throw new IllegalArgumentException("Trainer's surname is null.");
        }
        if(trainer.getBirthDate() == null){
            throw new IllegalArgumentException("Trainer's birthday is null.");
        }  
        
        try{
            Trainer find = em.find(Trainer.class, trainer.getId());
            if(find == null){
                throw new DAOException("Trainer " + trainer + " does not exist.");
            }
            em.merge(trainer);
        } catch (Exception ex){
            throw new DAOException("Trainer " + trainer + " not updated", ex);
        }        

    }

    /**
     * delete Trainer
     * @param trainer Trainer entity
     */
    public void deleteTrainer(Trainer trainer) throws IllegalArgumentException, DAOException {
        
        if(trainer == null){
            throw new IllegalArgumentException("Trainer is null.");
        }
        if(trainer.getId() == null){
            throw new IllegalArgumentException("Trainer ID is null");
        }
        
        try{
            Trainer find = em.find(Trainer.class, trainer.getId());
            if(find == null){
                throw new DAOException("Trainer " + trainer + " does not exist.");
            }
            em.remove(em.contains(trainer)?trainer:em.merge(trainer));
        } catch (Exception ex){
            throw new DAOException("Trainer " + trainer + " not deleted", ex);
        }        
    }

    /**
     * find Trainer in database by id
     * @param id Trainer identification
     * @return trainer from database
     */
    public Trainer findTrainerById(Long id) throws IllegalArgumentException, DAOException {
        
            if (id == null) {
                throw new IllegalArgumentException("Given ID is not in database");
            }
        
        try {
            //Trainer trainer = em.createQuery("SELECT e FROM Trainer e WHERE e.id= :id",Trainer.class).setParameter("id", id).getSingleResult();
            Trainer trainer = em.find(Trainer.class, id);
            
            if (trainer == null) {
                throw new IllegalArgumentException("Trainer with given id is not in database");
            } else {
                return trainer;
            }
        } catch (Exception e) {
            throw new DAOException("Error while finding a trainer.", e);
        }        
    }
    
    /**
     * find Trainer in database by name
     * @param name Trainer identification
     * @return trainer from database
     */
    public Trainer findTrainerByName(String name) throws IllegalArgumentException, DAOException {
                    if (name == null) {
                throw new IllegalArgumentException("Given name is not in database");
            }
        
        try {

            //Trainer trainer = em.find(Trainer.class, name);
            Trainer trainer = em.createQuery("SELECT e FROM Trainer e WHERE e.name= :name",Trainer.class).setParameter("name", name).getSingleResult();

            if (trainer == null) {
                throw new IllegalArgumentException("Trainer with given name is not in database");
            } else {
                return trainer;
            }
        } catch (Exception e) {
            throw new DAOException("Error while finding a trainer.", e);
        }       
    }   

    /**
     * Return list of all trainers
     * @return list of trainers
     */
    public List<Trainer> findAllTrainer() throws DAOException {      
        
        try {            
            return em.createQuery("SELECT e FROM Trainer e",Trainer.class).getResultList();

        } catch (Exception e) {
            throw new DAOException("Error while retrieving all trainers.", e);
        }
    }
    
}
