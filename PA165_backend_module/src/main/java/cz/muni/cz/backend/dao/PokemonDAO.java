/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.backend.dao;

import cz.muni.cz.backend.entities.Pokemon;
import property.Type;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Martin Prokop
 */
public interface PokemonDAO {
    
    /**
     * Setting manager context
     * @param manager 
     */
    public void setManagerContext(EntityManager manager);    
    
    /**
     * Create new pokemon
     * @param pokemon Pokemon to be created
     */
    public void createPokemon(Pokemon pokemon);
    
    /**
     * Update existing pokemon
     * @param pokemon Pokemon to be updated
     */
    public void updatePokemon(Pokemon pokemon);
    
    /**
     * Delete existing pokemon
     * @param pokemon Pokemon to be deleted
     */
    public void deletePokemon(Pokemon pokemon);
    
    /**
     * Find pokemon by id
     * @param id Id of desired pokemon
     * @return Pokemon
     */
    public Pokemon findPokemonById(Long id);
    
    /**
     * Find all pokemons
     * @return List of pokemons
     */
    public List<Pokemon> findAllPokemon();
    
    /**
     * Find pokemon by name
     * @param name Name of desired pokemon
     * @return List of pokemons which have the name
     */
    public List<Pokemon> findPokemonByName(String name);
    
    /**
     * Find pokemon by nick
     * @param name
     * @return Pokemon
     */
    public Pokemon findPokemonByNick(String name);
    
    /**
     * Find pokemon by type
     * @param type Type of desired pokemon
     * @return List of pokemons which have the type
     */
    public List<Pokemon> findPokemonByType(Type type);
    
    /**
     * Find pokemon by level
     * @param level Level of desired pokemon
     * @return List of pokemons which have the level
     */
    public List<Pokemon> findPokemonByLevel(int level);
    
    /**
     * Find pokemon by trainer
     * @param trainer Trainer of desired pokemon
     * @return List of pokemons which have the trainer
     */
    public List<Pokemon> findPokemonByTrainer(Trainer trainer);
    
    
}
