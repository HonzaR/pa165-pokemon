/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.backend.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Tom� Sl�dek, 359532 
 */

@Entity
public class Badge implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Trainer trainer;
    @ManyToOne
    private Stadium stadium;   
     
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Trainer getTrainer() {
        return trainer;
    }
    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
    public Stadium getStadium() {
        return stadium;
    }
    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 97 * hash + (this.trainer != null ? this.trainer.hashCode() : 0);
        hash = 97 * hash + (this.stadium != null ? this.stadium.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Badge other = (Badge) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.trainer != other.trainer && (this.trainer == null || !this.trainer.equals(other.trainer))) {
            return false;
        }
        return !(this.stadium != other.stadium && (this.stadium == null || !this.stadium.equals(other.stadium)));
    }

    @Override
    public String toString() {
        return "Badge{" + "id=" + id + ", trainer=" + trainer + ", stadium=" + stadium + '}';
    }

    
    
    
    
}
