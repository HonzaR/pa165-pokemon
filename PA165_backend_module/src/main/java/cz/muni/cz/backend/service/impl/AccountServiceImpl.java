package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.AccountDTO;
import cz.muni.cz.api.service.AccountService;
import cz.muni.cz.backend.dao.AccountDAO;
import cz.muni.cz.backend.entities.Account;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/*
 * Account service implementation
 *
 * @author: Tomas Sladek, 359532
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDAO accDAO;

    public void setAccountDAO(AccountDAO acc) {
        this.accDAO = acc;
    }

    public AccountDAO getAccountDAO() {
        return this.accDAO;
    }

    @Override
    public void create(AccountDTO dto) {

            Account acc = Convertor.accountDtoToEnt(dto);
            String passHash = new ShaPasswordEncoder(256).encodePassword(acc.getPassword(), null);
            acc.setPassword(passHash);
            accDAO.createAccount(acc);
    }

    public void update(AccountDTO dto) {

            Account acc = Convertor.accountDtoToEnt(dto);
            String passHash = new ShaPasswordEncoder(256).encodePassword(acc.getPassword(), null);
            acc.setPassword(passHash);
            accDAO.updateAccount(acc);
    }

    public void delete(AccountDTO dto) {

            Account acc = Convertor.accountDtoToEnt(dto);
            accDAO.deleteAccount(acc);

    }

    public List<AccountDTO> findAll() {
        List<Account> accounts = accDAO.findAll();
        List<AccountDTO> accountsDTO = new ArrayList<AccountDTO>();

        for (Account acc : accounts) {
            accountsDTO.add(Convertor.accountEntToDto(acc));
        }
        return accountsDTO;
    }

    public AccountDTO findById(Long id) {

            Account acc = accDAO.findAccountById(id);
            return Convertor.accountEntToDto(acc);
    }

    public AccountDTO findByName(String name) {

            Account acc = accDAO.findAccountByName(name);
            if (acc == null) {
                return null;
            } else {
                return Convertor.accountEntToDto(acc);
            }

    }

}
