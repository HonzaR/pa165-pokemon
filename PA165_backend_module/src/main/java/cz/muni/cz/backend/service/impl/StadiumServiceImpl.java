/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.StadiumService;
import cz.muni.cz.backend.dao.StadiumDAO;
import cz.muni.cz.backend.entities.Stadium;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import property.Type;

/**
 *
 * @author Tommy
 */
@Service
@Transactional
public class StadiumServiceImpl implements StadiumService {
    
    @Autowired
    private StadiumDAO stadiumDAO;
    
    /**
     * Set badgeDAO
     * @param stadiumDAO 
     */
    public void setStadiumDAO(StadiumDAO stadiumDAO)
    {
        this.stadiumDAO = stadiumDAO;
    }
    
    /**
     * Get badgeDAO
     * @return badgeDAO
     */
    public StadiumDAO getStadiumDAO()
    {
        return this.stadiumDAO;
    }

    /**
     * Create Stadium DTO
     * @param stadiumDTO
     */
    public void createStadium(StadiumDTO stadiumDTO) {

        Stadium stadium = Convertor.stadiumDtoToEnt(stadiumDTO);
        stadiumDAO.createStadium(stadium);
    }

    /**
     * Update Stadium DTO
     * @param stadiumDTO
     */
    public void updateStadium(StadiumDTO stadiumDTO) {

        Stadium stadium = Convertor.stadiumDtoToEnt(stadiumDTO);
        stadiumDAO.updateStadium(stadium);
    }

    /**
     * Delete Stadium DTO
     * @param stadiumDTO 
     */
    public void deleteStadium(StadiumDTO stadiumDTO) {

        Stadium stadium = Convertor.stadiumDtoToEnt(stadiumDTO);
        stadiumDAO.deleteStadium(stadium);
    }

    /**
     * Find Stadium DTO by ID
     * @param id
     * @return Stadium DTO
     */
    public StadiumDTO findStadiumById(Long id) {

        Stadium stadium = stadiumDAO.findStadiumById(id);
        return Convertor.stadiumEntToDto(stadium);
    }

    /**
     * Return all Stadium DTO
     * @return list of Stadium DTO
     */
    public List<StadiumDTO> findAllStadiums() {
        List<Stadium> stadiums = stadiumDAO.findAllStadiums();
        List<StadiumDTO> stadiumsDTO  = new ArrayList<StadiumDTO>();
        
        for(Stadium s : stadiums)
        {
            stadiumsDTO.add(Convertor.stadiumEntToDto(s));
        }
        return stadiumsDTO;
    }
    
    
    /**
     * Find stadium DTO by city
     * @param city
     * @return list of stadium
     */
    public List<StadiumDTO> findStadiumByCity(String city) {
        
        List<Stadium> stadiums = stadiumDAO.findStadiumByCity(city);
        List<StadiumDTO> stadiumsDTO = new ArrayList<StadiumDTO>();

        for(Stadium s : stadiums)
        {
            stadiumsDTO.add(Convertor.stadiumEntToDto(s));
        }
        return stadiumsDTO;
                
    }

    /**
     * Find stadium DTO by type
     * @param type
     * @return list of stadium
     */
    public List<StadiumDTO> findStadiumByType(Type type) {

        List<Stadium> stadiums = stadiumDAO.findStadiumByType(type);
        List<StadiumDTO> stadiumsDTO = new ArrayList<StadiumDTO>();
        
        for(Stadium s : stadiums)
        {
            stadiumsDTO.add(Convertor.stadiumEntToDto(s));
        }
        return stadiumsDTO;        
    }

    /**
     * Find stadium DTO by trainer
     * @param trainer
     * @return stadium
     */
    public StadiumDTO findStadiumByTrainer(TrainerDTO trainer) {

        Stadium stadium = stadiumDAO.findStadiumByTrainer(Convertor.trainerDtoToEnt(trainer));
            if(stadium != null){
                return Convertor.stadiumEntToDto(stadium);
            } else {
                return null;
            }
       
    }
    
}

