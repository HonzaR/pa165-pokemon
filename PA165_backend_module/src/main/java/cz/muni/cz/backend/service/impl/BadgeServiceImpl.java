/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.BadgeService;
import cz.muni.cz.backend.dao.BadgeDAO;
import cz.muni.cz.backend.entities.Badge;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Badge service implementation
 * @author Tomas Sekera, 359902
 */
@Service
@Transactional
public class BadgeServiceImpl implements BadgeService {
    
    @Autowired
    private BadgeDAO badgeDAO;
    
    /**
     * Set badgeDAO
     * @param badgeDAO 
     */
    public void setBadgeDAO(BadgeDAO badgeDAO)
    {
        this.badgeDAO = badgeDAO;
    }
    
    /**
     * Get badgeDAO
     * @return badgeDAO
     */
    public BadgeDAO getBadgeDAO()
    {
        return this.badgeDAO;
    }

    /**
     * Create Badge DTO
     * @param badgeDTO
     */
    public void createBadge(BadgeDTO badgeDTO) {           
        
        Badge badge = Convertor.badgeDtoToEnt(badgeDTO);
        badgeDAO.createBadge(badge); 
    }

    /**
     * Update Badge DTO
     * @param badgeDTO
     */
    public void updateBadge(BadgeDTO badgeDTO) {
        
        Badge badge = Convertor.badgeDtoToEnt(badgeDTO);
        badgeDAO.updateBadge(badge);
    }

    /**
     * Delete Badge DTO
     * @param badgeDTO 
     */
    public void deleteBadge(BadgeDTO badgeDTO) {

        Badge badge = Convertor.badgeDtoToEnt(badgeDTO);
        badgeDAO.deleteBadge(badge);
    }

    /**
     * Find Badge DTO by ID
     * @param id
     * @return Badge DTO
     */
    public BadgeDTO findBadgeById(Long id)  {

        Badge badge = badgeDAO.findBadgeById(id);
        return Convertor.badgeEntToDto(badge);
    }

    /**
     * Return all Badges DTO
     * @return list of Badges DTO
     */
    public List<BadgeDTO> findAllBadges() {
        List<Badge> badges = badgeDAO.findAllBadges();
        List<BadgeDTO> badgesDTO  = new ArrayList<BadgeDTO>();
        
        for(Badge bad : badges)
        {
            badgesDTO.add(Convertor.badgeEntToDto(bad));
        }
        return badgesDTO;
    }
    
    
    /**
     * Find badges DTO by stadium
     * @param stadiumDTO
     * @return list of badges
     */
    public List<BadgeDTO> findBadgeByStadium(StadiumDTO stadiumDTO) {

        List<Badge> badges = badgeDAO.findBadgeByStadium(Convertor.stadiumDtoToEnt(stadiumDTO));
        List<BadgeDTO> badgesDTO = new ArrayList<BadgeDTO>();
        
        for(Badge bad : badges)
        {
            badgesDTO.add(Convertor.badgeEntToDto(bad));
        }
        return badgesDTO;        
    }

    /**
     * Find badges DTO by trainer
     * @param trainerDTO
     * @return list of badges
     */
    public List<BadgeDTO> findBadgeByTrainer(TrainerDTO trainerDTO) {

        List<Badge> badges = badgeDAO.findBadgeByTrainer(Convertor.trainerDtoToEnt(trainerDTO));
        List<BadgeDTO> badgesDTO = new ArrayList<BadgeDTO>();
        
        for(Badge bad : badges)
        {
            badgesDTO.add(Convertor.badgeEntToDto(bad));
        }
        return badgesDTO;
    }
    
}
