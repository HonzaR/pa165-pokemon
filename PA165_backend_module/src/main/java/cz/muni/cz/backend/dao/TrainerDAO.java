package cz.muni.cz.backend.dao;

import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tomáš Sekera, 359902
 */
public interface TrainerDAO {
    
    
    /**
     * Setting manager context
     * @param manager 
     */
    public void setManagerContext(EntityManager manager);
    
    /**
     * Create new trainer
     * @param trainer Trainer to be created
     */
    public void createTrainer(Trainer trainer);
    
    /**
     * Update existing trainer
     * @param trainer Trainer to be updated
     */
    public void updateTrainer(Trainer trainer);
    
    /**
     * Delete existing trainer
     * @param trainer Trainer to be deleted
     */
    public void deleteTrainer(Trainer trainer);    
    
    /**
     * Find trainer by id
     * @param id Id of desired trainer
     * @return Trainer
     */
    public Trainer findTrainerById(Long id);
    
    /**
     * Find all trainers
     * @return List of trainers
     */
    public List<Trainer> findAllTrainer();
    
    /**
     * Find trainer by name
     * @param name Name of desired trainer
     * @return Trainer
     */
    public Trainer findTrainerByName(String name);
    
}
