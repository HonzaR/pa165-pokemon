package cz.muni.cz.backend.dao.impl;

import cz.muni.cz.backend.dao.StadiumDAO;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import property.Type;

/**
 *
 * @author Tomáš Sekera, 359902
 */

@Repository
public class StadiumDAOImpl implements StadiumDAO{
    
    
    @PersistenceContext
    private EntityManager em;
    
    
    public void setManagerContext(EntityManager manager)
    {
        this.em = manager;
    }
    
    
    public void setEMContext(EntityManager EM)
    {
        this.em = EM;
    }
    
    public void createStadium(Stadium stadium)
    {
        if (stadium == null)
            throw new IllegalArgumentException("Stadium is null");
        else if (stadium.getCity() == null)
            throw new IllegalArgumentException("Stadium city is null");
        else if (stadium.getType() == null)
            throw new IllegalArgumentException("Stadium type is null");
        else if (stadium.getTrainer() == null)
            throw new IllegalArgumentException("Stadium trainer is null");
        
        em.persist(stadium);
        
    }
    
    public void updateStadium(Stadium stadium)
    {
        if (stadium == null)
            throw new IllegalArgumentException("Stadium is null");
        else if (stadium.getId() == null)
            throw new IllegalArgumentException("Stadium id is null");
        else if (stadium.getCity() == null)
            throw new IllegalArgumentException("Stadium city is null");
        else if (stadium.getType() == null)
            throw new IllegalArgumentException("Stadium type is null");
        else if (stadium.getTrainer() == null)
            throw new IllegalArgumentException("Stadium trainer is null");
        
        em.merge(stadium);
        
    }
    
    public boolean deleteStadium(Stadium stadium)
    {
        if (stadium == null)
            throw new IllegalArgumentException("Stadium is null");
        else if (stadium.getId() == null)
            throw new IllegalArgumentException("Stadium id is null");

        try{
            Stadium find = em.find(Stadium.class, stadium.getId());
            if(find == null){
                throw new DAOException("Stadium " + stadium + " does not exist.");
            }
            em.remove(em.contains(stadium)?stadium:em.merge(stadium));
            return true;
        } catch (Exception ex){
            throw new DAOException("Stadium " + stadium + " not deleted", ex);
        } 
    }
    
    public Stadium findStadiumById(Long id)
    {
        if (id == null)
            throw new IllegalArgumentException("Stadium id is null");
        
        Stadium stadium = em.find(Stadium.class, id);
        
        if (stadium == null)
            throw new IllegalArgumentException("Given id is not in database");
        else
            return stadium;
    }
    
    public List<Stadium> findAllStadiums()
    {
        Query stadiumList =  em.createQuery("SELECT e FROM Stadium e");
        
        List<Stadium> stadium = stadiumList.getResultList();
        
        return stadium;
        
    }
    
    public Stadium findStadiumByTrainer(Trainer trainer)
    {
        if (trainer == null)
            throw new IllegalArgumentException("Trainer is null");
        
        List<Stadium> stadium =  em.createQuery("SELECT e FROM Stadium e WHERE e.trainer= :trainer").setParameter("trainer", trainer).getResultList();
        
        if(!stadium.isEmpty()){
            return stadium.get(0);
        } else {
            return null;
        }
        
    }
    
    public List<Stadium> findStadiumByCity(String city)
    {
        if (city == null)
            throw new IllegalArgumentException("City is null");
        
        List<Stadium> stadium =  em.createQuery("SELECT e FROM Stadium e WHERE e.city= :city").setParameter("city", city).getResultList();
        
        return stadium;
    }

    
    public List<Stadium> findStadiumByType(Type type)
    {
        if (type == null)
            throw new IllegalArgumentException("Type is null");
        
        List<Stadium> stadium =  em.createQuery("SELECT e FROM Stadium e WHERE e.type= :type").setParameter("type", type).getResultList();
        
        return stadium;
    }
    
}
