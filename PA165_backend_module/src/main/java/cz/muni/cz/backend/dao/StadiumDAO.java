/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.backend.dao;

import property.Type;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Jan Rychnovsk�, 430642
 */
public interface StadiumDAO {
    
    /**
     * Setting manager context
     * @param manager 
     */
    public void setManagerContext(EntityManager manager);    

    /**
     * add new Stadium into the database
     * @param stadium Stadium entity
     * @return true/false
     */
    public void createStadium(Stadium stadium);

    /**
     * update Stadium in database
     * @param stadium Stadium entity
     * @return true/false
     */
    public void updateStadium(Stadium stadium);

    /**
     * remove Stadium from database
     * @param stadium Stadium entity
     * @return true/false
     */
    public boolean deleteStadium(Stadium stadium);

    /**
     * find Stadium in database based on id
     * @param id Stadium identification
     * @return stadium
     * @return Stadium
     */
    public Stadium findStadiumById(Long id);
    
    /**
     * find Stadium in database based on city
     * @param city in which is stadium located
     * @return stadium
     */
    public List<Stadium> findStadiumByCity(String city);
    
    /**
     * find Stadium in database based on type
     * @param type of stadium
     * @return stadium
     */
    public List<Stadium> findStadiumByType(Type type);

    /**
     * find Stadium in database based on stadium trainer
     * @param trainer of Stadium
     * @return stadium
     */
    public Stadium findStadiumByTrainer(Trainer trainer);    
    
    /**
     * Return list of all stadiums
     * @return list of stadiums
     */
    public List<Stadium> findAllStadiums();

}
