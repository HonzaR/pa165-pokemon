/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.PokemonService;
import cz.muni.cz.backend.dao.PokemonDAO;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Trainer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import property.Type;


/**
 * Implementation of service for Pokemon entity
 * 
 * @author Jan Rychnovský
 */
@Service
@Transactional
public class PokemonServiceImpl implements PokemonService {

    @Autowired
    private PokemonDAO pokemonDAO;

     /**
     * Set the parameter pokemonDAO value
     *
     * @param pokemonDAO Pokemon as Data Access Object
      */
    public void setPokemonDAO(PokemonDAO pokemonDAO) {
        this.pokemonDAO = pokemonDAO;
    }

     /**
     * Get the parameter pokemonDAO value
     *
     * @return pokemonDAO Pokemon as Data Access Object
      */
    public PokemonDAO getPokemonDAO() {
        return this.pokemonDAO;
    }

    /**
     * Convert Pokemon DTO into Pokemon entity and perform method add Pokemon
     * with entity on DAO layer
     *
     * @param pokemonDTO Pokemon as DTO
     */
    public void createPokemon(PokemonDTO pokemonDTO) {
        
        Pokemon pokemonEnt = Convertor.pokemonDtoToEnt(pokemonDTO);
        pokemonDAO.createPokemon(pokemonEnt);                    
    }

    /**
     * Convert Pokemon DTO into Pokemon entity and perform method update Pokemon
     * with entity on DAO layer
     *
     * @param pokemonDTO Pokemon as DTO
     */
    public void updatePokemon(PokemonDTO pokemonDTO) {
        
        Pokemon pokemonEnt = Convertor.pokemonDtoToEnt(pokemonDTO);
        pokemonDAO.updatePokemon(pokemonEnt);             
    }

    /**
     * Convert Pokemon DTO into Pokemon entity and perform method delete Pokemon
     * with entity on DAO layer
     *
     * @param pokemonDTO Pokemon as DTO
     */
    public void deletePokemon(PokemonDTO pokemonDTO) {
        
        Pokemon pokemonEnt = Convertor.pokemonDtoToEnt(pokemonDTO);
        pokemonDAO.deletePokemon(pokemonEnt);                   
    }
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonById
     * with entity on DAO layer
     *
     * @param id
     * @return pokemonDTO Pokemon as DTO
     */
    public PokemonDTO findPokemonById(Long id) {
        
        Pokemon pokemonEnt = pokemonDAO.findPokemonById(id);
        return Convertor.pokemonEntToDto(pokemonEnt);            
    } 
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findAllPokemon
     * with entity on DAO layer
     *
     * @return list of pokemonDTO objects
     */
    public List<PokemonDTO> findAllPokemon() {
        
        List<Pokemon> pokemonEnt = pokemonDAO.findAllPokemon();
        List<PokemonDTO> pokemonDTOList = new ArrayList<PokemonDTO>();
        
        for (Pokemon pok : pokemonEnt) {
            pokemonDTOList.add(Convertor.pokemonEntToDto(pok));
        }
        return pokemonDTOList;
    }  
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonByName
     * with entity on DAO layer
     *
     * @param name
     * @return list of pokemonDTO objects
     */
    public List<PokemonDTO> findPokemonByName(String name) {
        
        List<Pokemon> pokemonEnt = pokemonDAO.findPokemonByName(name);
        List<PokemonDTO> pokemonDTOList = new ArrayList<PokemonDTO>();
        for (Pokemon pok : pokemonEnt) {
            pokemonDTOList.add(Convertor.pokemonEntToDto(pok));
        }
        return pokemonDTOList;        
    } 
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonByNick
     * with entity on DAO layer
     *
     * @param nick
     * @return pokemonDTO Pokemon as DTO
     */
    public PokemonDTO findPokemonByNick(String nick) {
        
        Pokemon pokemonEnt = pokemonDAO.findPokemonByNick(nick);
        return Convertor.pokemonEntToDto(pokemonEnt);            
    } 
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonByType
     * with entity on DAO layer
     *
     * @param type
     * @return list of pokemonDTO objects
     */
    public List<PokemonDTO> findPokemonByType(Type type) {
        
        List<Pokemon> pokemonEnt = pokemonDAO.findPokemonByType(type);
        List<PokemonDTO> pokemonDTOList = new ArrayList<PokemonDTO>();
        for (Pokemon pok : pokemonEnt) {
            pokemonDTOList.add(Convertor.pokemonEntToDto(pok));
        }
        return pokemonDTOList;        
    }     
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonByLevel
     * with entity on DAO layer
     *
     * @param level
     * @return list of pokemonDTO objects
     */
    public List<PokemonDTO> findPokemonByLevel(int level) {
                
        List<Pokemon> pokemonEnt = pokemonDAO.findPokemonByLevel(level);
        List<PokemonDTO> pokemonDTOList = new ArrayList<PokemonDTO>();
        for (Pokemon pok : pokemonEnt) {
            pokemonDTOList.add(Convertor.pokemonEntToDto(pok));
        }
        return pokemonDTOList;        
    } 
    
    /**
     * Convert Pokemon DTO into Pokemon entity and perform method findPokemonByTrainer
     * with entity on DAO layer
     *
     * @param trainerDTO
     * @return list of pokemonDTO objects
     */
    public List<PokemonDTO> findPokemonByTrainer(TrainerDTO trainerDTO) {
        
        Trainer trainer = Convertor.trainerDtoToEnt(trainerDTO);
        List<Pokemon> pokemonEnt = pokemonDAO.findPokemonByTrainer(trainer);
        List<PokemonDTO> pokemonDTOList = new ArrayList<PokemonDTO>();
        for (Pokemon pok : pokemonEnt) {
            pokemonDTOList.add(Convertor.pokemonEntToDto(pok));
        }
        return pokemonDTOList;        
    }     

}