/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.AccountDTO;
import cz.muni.cz.api.dto.BadgeDTO;
import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.StadiumDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.backend.entities.Account;
import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;

/**
 * Convert DTO and Entity objects
 * 
 * @author Jan Rychnovský
 */
public class Convertor {
    

    /**
     * Convert Pokemon Entity to DTO
     *
     * @param pokemon Pokemon as Entity
     * @return Pokemon as DTO
     */
    public static PokemonDTO pokemonEntToDto(Pokemon pokemon) {
        PokemonDTO pok = new PokemonDTO();
        TrainerDTO trn = new TrainerDTO();
        pok.setName(pokemon.getName());
        pok.setNick(pokemon.getNick());
        pok.setType(pokemon.getType());
        pok.setLevel(pokemon.getLevel());
        if (pokemon.getTrainer() == null) {
            pok.setTrainer(null);
        } else {
            pok.setTrainer(trainerEntToDto(pokemon.getTrainer()));
        }
        pok.setId(pokemon.getId());
        return pok;
    }

    /**
     * Convert Pokemon DTO to Entity
     *
     * @param pokemonDTO
     * @return Pokemon as Entity
     */
    public static Pokemon pokemonDtoToEnt(PokemonDTO pokemonDTO) {
        Pokemon pok = new Pokemon();
        pok.setName(pokemonDTO.getName());
        pok.setNick(pokemonDTO.getNick());
        pok.setType(pokemonDTO.getType());
        pok.setLevel(pokemonDTO.getLevel());
        if (pokemonDTO.getTrainer() == null) {
            pok.setTrainer(null);
        } else {
            pok.setTrainer(trainerDtoToEnt(pokemonDTO.getTrainer()));
        }
        pok.setId(pokemonDTO.getId());
        return pok;
    }
    
    public static TrainerDTO trainerEntToDto(Trainer trainer) {
        TrainerDTO trainerDTO = new TrainerDTO();
        
        trainerDTO.setName(trainer.getName());
        trainerDTO.setSurName(trainer.getSurName());
        trainerDTO.setBirthDate(trainer.getBirthDate());
        trainerDTO.setId(trainer.getId());
        
        return trainerDTO;
    }
    
    public static Trainer trainerDtoToEnt(TrainerDTO trainerDTO) {    
        Trainer trainer = new Trainer();
        
        trainer.setName(trainerDTO.getName());
        trainer.setSurName(trainerDTO.getSurName());
        trainer.setBirthDate(trainerDTO.getBirthDate());
        trainer.setId(trainerDTO.getId());
        
        return trainer;
    }
    
    /**
     *  Convert Badge entity to DTO
     * @param badge
     * @return DTO badge
     */
    public static BadgeDTO badgeEntToDto(Badge badge)
    {
        BadgeDTO badgeDTO = new BadgeDTO();
        
        badgeDTO.setId(badge.getId());
        
        //stadium
        if (badge.getStadium() == null) {
            badgeDTO.setStadium(null);
        } else {
            badgeDTO.setStadium(stadiumEntToDto(badge.getStadium()));
        }
        
        //trainer
        if (badge.getTrainer() == null) {
            badgeDTO.setTrainer(null);
        } else {
            badgeDTO.setTrainer(trainerEntToDto(badge.getTrainer()));
        }
        
        return badgeDTO;
    }
    
    
    /**
     * Convert Badge DTO to entity
     * @param badgeDTO
     * @return badge
     */
    public static Badge badgeDtoToEnt(BadgeDTO badgeDTO)
    {
        Badge badge = new Badge();
        
        badge.setId(badgeDTO.getId());
        
        //stadium
        if (badgeDTO.getStadium() == null) {
            badge.setStadium(null);
        } else {
            badge.setStadium(stadiumDtoToEnt(badgeDTO.getStadium()));
        }
        
        //trainer
        if (badgeDTO.getTrainer() == null) {
            badge.setTrainer(null);
        } else {
            badge.setTrainer(trainerDtoToEnt(badgeDTO.getTrainer()));
        }
    
        return badge;
    }
    
    
    /**
     *  Convert Stadium entity to DTO
     * @param stadium
     * @return stadium DTO
     */
    public static StadiumDTO stadiumEntToDto(Stadium stadium)
    {
        StadiumDTO stadiumDTO = new StadiumDTO();       
        stadiumDTO.setId(stadium.getId());       
        //city
        if (stadium.getCity() == null) {
            stadiumDTO.setCity(null);
        } else {
            stadiumDTO.setCity(stadium.getCity());
        }       
        //type
        if (stadium.getType() == null) {
            stadiumDTO.setType(null);
        } else {
            stadiumDTO.setType(stadium.getType());
        }        
        //trainer
        if (stadium.getTrainer() == null) {
            stadiumDTO.setTrainer(null);
        } else {
            stadiumDTO.setTrainer(trainerEntToDto(stadium.getTrainer()));
        }      
        return stadiumDTO;
    }
    
    /**
     * Convert Stadium DTO to entity
     * @param stadiumDTO
     * @return stadium
     */
    public static Stadium stadiumDtoToEnt(StadiumDTO stadiumDTO)
    {
        Stadium stadium = new Stadium();        
        stadium.setId(stadiumDTO.getId());        
        //stadium
        if (stadiumDTO.getTrainer() == null) {
            stadium.setTrainer(null);
        } else {
            stadium.setTrainer(trainerDtoToEnt(stadiumDTO.getTrainer()));
        }        
        //city
        if (stadiumDTO.getCity() == null) {
            stadium.setCity(null);
        } else {
            stadium.setCity(stadiumDTO.getCity());
        }        
        //type
        if (stadiumDTO.getType() == null) {
            stadium.setType(null);
        } else {
            stadium.setType(stadiumDTO.getType());
        }        
        return stadium;
    }

    public static Account accountDtoToEnt(AccountDTO dto) {
        Account acc = new Account();        
        acc.setId(dto.getId());        
        //name
        if (dto.getName() == null) {
            acc.setName(null);
        } else {
            acc.setName(dto.getName());
        } 
        //password
        if (dto.getPassword() == null) {
            acc.setPassword(null);
        } else {
            acc.setPassword(dto.getPassword());
        } 
        //trainer ID
        if (dto.getTrainerID()== null) {
            acc.setTrainerID(null);
        } else {
            acc.setTrainerID(dto.getTrainerID());
        }
        //isAdmin
        if (dto.getIsAdmin()== null) {
            acc.setIsAdmin(null);
        } else {
            acc.setIsAdmin(dto.getIsAdmin());
        }      
        return acc;
    }

    static AccountDTO accountEntToDto(Account acc) {
        AccountDTO dto = new AccountDTO();        
        dto.setId(acc.getId());        
        //name
        if (acc.getName() == null) {
            dto.setName(null);
        } else {
            dto.setName(acc.getName());
        } 
        //password
        if (acc.getPassword() == null) {
            dto.setPassword(null);
        } else {
            dto.setPassword(acc.getPassword());
        } 
        //trainer ID
        if (acc.getTrainerID()== null) {
            dto.setTrainerID(null);
        } else {
            dto.setTrainerID(acc.getTrainerID());
        }
        //isAdmin
        if (acc.getIsAdmin()== null) {
            dto.setIsAdmin(null);
        } else {
            dto.setIsAdmin(acc.getIsAdmin());
        }      
        return dto;
    }
 
}
