/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cz.muni.cz.backend.service.impl;

import cz.muni.cz.api.dto.PokemonDTO;
import cz.muni.cz.api.dto.TrainerDTO;
import cz.muni.cz.api.service.TrainerService;
import cz.muni.cz.backend.dao.TrainerDAO;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Trainer;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service for Trainer entity
 * 
 * @author Martin Prokop
 */
@Service
@Transactional
public class TrainerServiceImpl implements TrainerService{
    
    @Autowired
    private TrainerDAO trainerDAO;

     /**
     * Set the parameter TrainerDAO value
     *
     * @param trainerDAO Trainer as Data Access Object
      */
    public void setTrainerDAO(TrainerDAO trainerDAO) {
        this.trainerDAO = trainerDAO;
    }

     /**
     * Get the parameter trainerDAO value
     *
     * @return trainerDAO Trainer as Data Access Object
      */
    public TrainerDAO getTrainerDAO() {
        return this.trainerDAO;
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method createTrainer
     * with entity on DAO layer
     *
     * @param trainerDTO Trainer as DTO
     */
    public void createTrainer(TrainerDTO trainerDTO) {
          
        Trainer trainerEnt = Convertor.trainerDtoToEnt(trainerDTO);
        trainerDAO.createTrainer(trainerEnt);
       
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method updateTrainer
     * with entity on DAO layer
     *
     * @param trainerDTO Trainer as DTO
     */
    public void updateTrainer(TrainerDTO trainerDTO) {

        Trainer trainerEnt = Convertor.trainerDtoToEnt(trainerDTO);
        trainerDAO.updateTrainer(trainerEnt);
  
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method deleteTrainer
     * with entity on DAO layer
     *
     * @param trainerDTO Trainer as DTO
     */
    public void deleteTrainer(TrainerDTO trainerDTO) {

        Trainer trainerEnt = Convertor.trainerDtoToEnt(trainerDTO);
        trainerDAO.deleteTrainer(trainerEnt);
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method findTrainerById
     * with entity on DAO layer
     *
     * @param id
     * @return TrainerDTO Trainer as DTO
     */
    public TrainerDTO findTrainerById(Long id) {

        Trainer trainerEnt = trainerDAO.findTrainerById(id);
        return Convertor.trainerEntToDto(trainerEnt);   
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method findAllTrainer
     * with entity on DAO layer
     *
     * @return list of TrainerDTO objects
     */
    public List<TrainerDTO> findAllTrainer() {
        List<Trainer> trainerEnt = trainerDAO.findAllTrainer();
        List<TrainerDTO> trainerDTOList = new ArrayList<TrainerDTO>();
        
        for (Trainer trainer : trainerEnt) {
            trainerDTOList.add(Convertor.trainerEntToDto(trainer));
        }
        return trainerDTOList;    
    }

    /**
     * Convert Trainer DTO into Trainer entity and perform method findTrainerByName
     * with entity on DAO layer
     *
     * @param name
     * @return list of TrainerDTO objects
     */
    public TrainerDTO findTrainerByName(String name) {
           
        Trainer trainerEnt = trainerDAO.findTrainerByName(name);
        return Convertor.trainerEntToDto(trainerEnt);
    }
    
}
