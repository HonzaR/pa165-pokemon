package cz.muni.cz.backend.dao.impl;

import cz.muni.cz.backend.dao.PokemonDAO;
import cz.muni.cz.backend.entities.Pokemon;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import property.Type;

/**
 *
 * @author Tomas Sladek, 359532
 */
@Repository
public class PokemonDAOImpl implements PokemonDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public void setManagerContext(EntityManager manager)
    {
        this.em = manager;
    }

    /**
     * Return entity manager factory
     * @return emf Entity Manager Factory
     */
    public EntityManager getEm() {
        return em;
    }

    /**
     * Set new entity manager factory
     * @param emf Entity Manager Factory
     */
    private void setEm(EntityManager em) {
        this.em = em;
    }  

    public void createPokemon(Pokemon pokemon) throws IllegalArgumentException, DAOException {
        
        if (pokemon == null)
            throw new IllegalArgumentException("Pokemon is null");
        if(pokemon.getId() != null)
            throw new IllegalArgumentException("Pokemon ID is already set.");
        if (pokemon.getClass()== null)
            throw new IllegalArgumentException("Pokemon class is null");
        if (pokemon.getType()== null)
            throw new IllegalArgumentException("Pokemon type is null");
        if (pokemon.getTrainer() == null)
            throw new IllegalArgumentException("Pokemon trainer is null");
        if (pokemon.getName()== null)
            throw new IllegalArgumentException("Pokemon name is null");
        if (pokemon.getNick()== null)
            throw new IllegalArgumentException("Pokemon nickname is null");
        
        try{
            em.persist(pokemon);
        } catch (Exception ex){
            throw new DAOException("Pokemon " + pokemon + " not created", ex);
        }
         
    }

    public void updatePokemon(Pokemon pokemon) {
        
        if (pokemon == null)
            throw new IllegalArgumentException("Pokemon is null");
        else if (pokemon.getId()== null)
            throw new IllegalArgumentException("Pokemon id is null");
        else if (pokemon.getClass()== null)
            throw new IllegalArgumentException("Pokemon class is null");
        else if (pokemon.getType()== null)
            throw new IllegalArgumentException("Pokemon type is null");
        else if (pokemon.getTrainer() == null)
            throw new IllegalArgumentException("Pokemon trainer is null");
        else if (pokemon.getName()== null)
            throw new IllegalArgumentException("Pokemon name is null");
        else if (pokemon.getNick()== null)
            throw new IllegalArgumentException("Pokemon nickname is null");
        
        em.merge(pokemon);
    }

    public void deletePokemon(Pokemon pokemon) {
        
        if (pokemon == null)
            throw new IllegalArgumentException("Pokemon is null");
        if (pokemon.getId()== null)
            throw new IllegalArgumentException("Pokemon id is null");
        
        try{
            Pokemon find = em.find(Pokemon.class, pokemon.getId());
            if(find == null){
                throw new DAOException("Pokemon " + pokemon + " does not exist.");
            }
            em.remove(em.contains(pokemon)?pokemon:em.merge(pokemon));
        } catch (Exception ex){
            throw new DAOException("Pokemon " + pokemon + " not deleted", ex);
        } 
    }

    public Pokemon findPokemonById(Long id) {
        
        if (id == null) {
                throw new IllegalArgumentException("Given ID is not in database");
            }
        
        try {
            Pokemon pokemon = em.find(Pokemon.class, id);
            
            if (pokemon == null) {
                throw new IllegalArgumentException("Pokemon with given id is not in database");
            } else {
                return pokemon;
            }
        } catch (Exception e) {
            throw new DAOException("Error while finding a pokemon.", e);
        }   
    }

    public List<Pokemon> findAllPokemon() {
        Query allPokemon = em.createQuery("SELECT p FROM Pokemon p");
        List<Pokemon> pokemon = allPokemon.getResultList();
        return pokemon;
    }

    public List<Pokemon> findPokemonByName(String name) {
        
        if (name == null)
            throw new IllegalArgumentException("Name is null");
        
        List<Pokemon> pokemon = em.createQuery("SELECT p FROM Pokemon p WHERE p.name= :name").setParameter("name", name).getResultList();
        return pokemon;
    }

    public Pokemon findPokemonByNick(String name) {
        
        if (name == null)
            throw new IllegalArgumentException("Nickname is null");
        
        List<Pokemon> pokemon = em.createQuery("SELECT p FROM Pokemon p WHERE p.nick= :name").setParameter("name", name).getResultList();
        return pokemon.get(0);
    }

    public List<Pokemon> findPokemonByType(Type type) {
        
        if (type == null)
            throw new IllegalArgumentException("Type is null");
        
        List<Pokemon> pokemon = em.createQuery("SELECT p FROM Pokemon p WHERE p.type= :type").setParameter("type", type).getResultList();
        return pokemon;
    }

    public List<Pokemon> findPokemonByLevel(int level) {             
        List<Pokemon> pokemon = em.createQuery("SELECT p FROM Pokemon p WHERE p.level= :level").setParameter("level", level).getResultList();
        return pokemon;
    }

    public List<Pokemon> findPokemonByTrainer(Trainer trainer) {
        
        if (trainer == null)
            throw new IllegalArgumentException("Trainer is null");
        
         List<Pokemon> pokemon = em.createQuery("SELECT p FROM Pokemon p WHERE p.trainer= :trainer").setParameter("trainer", trainer).getResultList();
         return pokemon;
    }
}
