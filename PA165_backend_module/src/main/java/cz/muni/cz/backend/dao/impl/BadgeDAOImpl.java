package cz.muni.cz.backend.dao.impl;

import cz.muni.cz.backend.dao.BadgeDAO;
import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Martin Prokop
 */
@Repository
public class BadgeDAOImpl implements BadgeDAO{
    
    @PersistenceContext
    private EntityManager manager;
    
    public void setManagerContext(EntityManager manager)
    {
        this.manager = manager;
    }

    public void createBadge(Badge badge) 
            throws DAOException, IllegalArgumentException{
        if(badge == null){
            throw new IllegalArgumentException("Badge is null.");
        }
        if(badge.getId() != null){
            throw new IllegalArgumentException("Badge ID is already set.");
        }
        if(badge.getTrainer() == null){
            throw new IllegalArgumentException("Badge trainer is null");
        }
        if(badge.getStadium() == null){
            throw new IllegalArgumentException("Badge stadium is null");
        }
        try{
            manager.persist(badge);
        } catch (Exception ex){
            throw new DAOException("Badge " + badge + " not created", ex);
        }
    }
    
    public void updateBadge(Badge badge) 
            throws DAOException, IllegalArgumentException{
        if(badge == null){
            throw new IllegalArgumentException("Badge is null.");
        }
        if(badge.getId() == null){
            throw new IllegalArgumentException("Badge ID is null");
        }
        if(badge.getTrainer() == null){
            throw new IllegalArgumentException("Badge trainer is null");
        }
        if(badge.getStadium() == null){
            throw new IllegalArgumentException("Badge stadium is null");
        }
        
        try{
            Badge find = manager.find(Badge.class, badge.getId());
            if(find == null){
                throw new DAOException("Badge " + badge + " does not exist.");
            }
            manager.merge(badge);
        } catch (Exception ex){
            throw new DAOException("Badge " + badge + " not updated", ex);
        }
    }
    
    public void deleteBadge(Badge badge) 
            throws DAOException, IllegalArgumentException{
        if(badge == null){
            throw new IllegalArgumentException("Badge is null.");
        }
        if(badge.getId() == null){
            throw new IllegalArgumentException("Badge ID is null");
        }
        
        try{
            Badge find = manager.find(Badge.class, badge.getId());
            if(find == null){
                throw new DAOException("Badge " + badge + " does not exist.");
            }
            manager.remove(manager.contains(badge)?badge:manager.merge(badge));
        } catch (Exception ex){
            throw new DAOException("Badge " + badge + " not deleted", ex);
        }
    }
    
    public Badge findBadgeById(Long id) 
            throws DAOException, IllegalArgumentException{
        if(id == null){
            throw new IllegalArgumentException("Cannot find badge, id is null");
        }
        
        try{
            Badge find = manager.find(Badge.class, id);
            if(find == null){
                throw new DAOException("Badge with id " + id + " does not exist.");
            }
            return find;
        } catch (Exception ex){
            throw new DAOException("Badge with id " + id + " not found", ex);
        }
    }
    
    public List<Badge> findAllBadges() 
            throws DAOException{
        List<Badge> badges;
        try{
            badges = manager.createQuery("SELECT b FROM Badge b").getResultList();
            return badges;
        } catch(Exception ex){
            throw new DAOException("All badges not found",ex);
        }
    }
    
    public List<Badge> findBadgeByStadium(Stadium stadium) 
            throws DAOException, IllegalArgumentException{
        if(stadium == null){
            throw new IllegalArgumentException("Cannot find badge, stadium is null");
        }
        
        try{
            List<Badge> find = manager.createQuery("SELECT b FROM Badge b WHERE b.stadium= :stadium").setParameter("stadium", stadium).getResultList();
            if(find == null){
                throw new DAOException("Badge with stadium " + stadium + " does not exist.");
            }
            return find;
        } catch (Exception ex){
            throw new DAOException("Badge with stadium " + stadium + " not found", ex);
        }
    }
    
    public List<Badge> findBadgeByTrainer(Trainer trainer) 
            throws DAOException, IllegalArgumentException{
        if(trainer == null){
            throw new IllegalArgumentException("Cannot find badge, trainer is null");
        }
        
        try{
            List<Badge> find = manager.createQuery("SELECT b FROM Badge b WHERE b.trainer= :trainer").setParameter("trainer", trainer).getResultList();
            if(find == null){
                throw new DAOException("Badge with stadium " + trainer + " does not exist.");
            }
            return find;
        } catch (Exception ex){
            throw new DAOException("Badge with stadium " + trainer + " not found", ex);
        }
    }
}
