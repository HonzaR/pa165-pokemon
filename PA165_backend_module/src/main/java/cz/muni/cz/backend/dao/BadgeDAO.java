/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.cz.backend.dao;

import cz.muni.cz.backend.entities.Badge;
import cz.muni.cz.backend.entities.Stadium;
import cz.muni.cz.backend.entities.Trainer;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tomas Sladek, 359532
 */
public interface BadgeDAO {
     /**
     * Create new badge
     * @param badge Badge to be created
     */
    public void createBadge(Badge badge);
    
    /**
     * Update existing badge
     * @param pokemon Badge to be updated
     */
    public void updateBadge(Badge pokemon);
    
    /**
     * Delete existing badge
     * @param pokemon Badge to be deleted
     */
    public void deleteBadge(Badge pokemon);
    
    /**
     * Find Badge by id
     * @param id Id of desired badge
     * @return Badge    
     */
    public Badge findBadgeById(Long id);
    
    /**
     * Find all badges
     * @return List of badges
     */
    public List<Badge> findAllBadges();
    
    /**
     * List badges by stadium
     * @param stadium Desired stadium
     * @return List of badges which come from the same stadium
     */
    public List<Badge> findBadgeByStadium(Stadium stadium);
    
    /**
     * List badges by trainer
     * @param trainer Desired trainer
     * @return List of badges which come from the same trainer
     */
    public List<Badge> findBadgeByTrainer(Trainer trainer);
    
    /**
     * Setting manager context
     * @param manager 
     */
    public void setManagerContext(EntityManager manager);
}
